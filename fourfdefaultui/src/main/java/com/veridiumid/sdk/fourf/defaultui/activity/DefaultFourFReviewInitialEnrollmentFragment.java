package com.veridiumid.sdk.fourf.defaultui.activity;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.veridiumid.sdk.fourf.defaultui.R;
import com.veridiumid.sdk.support.base.VeridiumBaseFragment;

public class DefaultFourFReviewInitialEnrollmentFragment extends VeridiumBaseFragment {

    protected Button test;
    protected Button cancel;
    protected RelativeLayout root;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(com.veridiumid.sdk.fourf.defaultui.R.layout.layout_4f_review, container, false);
    }

    @Override
    protected void initView(View view) {
        super.initView(view);

        root = (RelativeLayout) view.findViewById(com.veridiumid.sdk.fourf.defaultui.R.id.layout_4f_review_root);
        test = (Button) view.findViewById(com.veridiumid.sdk.fourf.defaultui.R.id.layout_4f_review_button_test);
        cancel = (Button) view.findViewById(com.veridiumid.sdk.fourf.defaultui.R.id.layout_4f_review_button_retry);

        root.setBackgroundColor(UICustomization.getBackgroundColor());
        test.setTextColor(UICustomization.getForegroundColor());
        cancel.setTextColor(UICustomization.getForegroundColor());

        TextView fourfReviewTitle = (TextView) view.findViewById(R.id.activity_fourf_tv_review_title);
        TextView fourfReviewSubtitle = (TextView) view.findViewById(R.id.activity_fourf_review_subtitle);
        fourfReviewTitle.setTextColor(UICustomization.getForegroundColor());
        fourfReviewSubtitle.setTextColor(UICustomization.getForegroundColor());


        DefaultFourFBiometricsActivity parentActivity = ((DefaultFourFBiometricsActivity) baseActivity);
        parentActivity.onReviewFragmentReady(this);
    }
}
