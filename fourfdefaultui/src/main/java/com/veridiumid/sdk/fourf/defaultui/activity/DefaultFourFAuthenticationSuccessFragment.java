package com.veridiumid.sdk.fourf.defaultui.activity;

/**
 * Created by tyson on 07/09/2016.
 */
import android.os.Bundle;
import android.provider.Contacts;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.veridiumid.sdk.support.base.VeridiumBaseFragment;
import com.veridiumid.sdk.fourf.defaultui.R;

public class DefaultFourFAuthenticationSuccessFragment extends VeridiumBaseFragment {

    protected Button bt_OK;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.layout_4f_auth_successful, container, false);
    }

    @Override
    public void initView(View view){
        super.initView(view);

        View base = (View) view.findViewById(R.id.activity_fourf_authsuccessful_base_layout);
        bt_OK = (Button) view.findViewById(R.id.button_ok);
        TextView tv_heading = (TextView) view.findViewById(R.id.activity_fourf_authsuccessful_tv_heading);

        base.setBackgroundColor(UICustomization.getBackgroundColor());
        bt_OK.setTextColor(UICustomization.getForegroundColor());
        tv_heading.setTextColor(UICustomization.getForegroundColor());

        DefaultFourFBiometricsActivity parentActivity = ((DefaultFourFBiometricsActivity) baseActivity);
        parentActivity.onAuthSuccessFragmentReady(this);
    }
}