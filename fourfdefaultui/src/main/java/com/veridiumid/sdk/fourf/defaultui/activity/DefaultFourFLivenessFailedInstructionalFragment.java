package com.veridiumid.sdk.fourf.defaultui.activity;

import android.animation.ValueAnimator;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.veridiumid.sdk.fourf.FourFInterface;
import com.veridiumid.sdk.fourf.defaultui.R;
import com.veridiumid.sdk.imaging.AnimationHelper;
import com.veridiumid.sdk.support.base.VeridiumBaseFragment;

/**
 * Created by lewiscarney on 07/04/2017.
 */

public class DefaultFourFLivenessFailedInstructionalFragment extends VeridiumBaseFragment {

    protected Button btn_getStarted;
    protected Button btn_cancel;

    protected ImageView texture;

    protected TextView tv_animationInfo;
    protected TextView tv_failed;

    protected ImageView iv_leftHand;
    protected ImageView iv_rightHand;
    protected ImageView iv_background;
    protected ImageView iv_phoneBackground;
    protected ImageView iv_phoneHand;
    protected ImageView iv_phoneGuide;
    protected ImageView iv_phoneGuideLiveness;
    protected ImageView iv_phoneGuideUp;
    protected ImageView iv_phoneGuideDown;
    protected ImageView iv_phoneROIs;
    protected ImageView iv_phoneColours;
    protected ImageView iv_flashOverlay;
    protected ImageView iv_phoneSuccess;

    private final static int ANIMATION_FLASH_DURATION = 200;
    private final static int ANIMATION_FLASH_TIME_BETWEEN = 50;

    private boolean firstTime = true;

    private int phoneHeight;
    private int phoneWidth;

    private boolean completed = false;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.layout_4f_liveness_instructional_animation, container, false);
    }

    @Override
    protected void initView(View view) {

        super.initView(view);
        int value = getArguments().getInt("livenessType");
        FourFInterface.LivenessType livenessType = FourFInterface.LivenessType.resolve(value);

        btn_getStarted = (Button) view.findViewById(R.id.activity_fourf_instructional_button_begin);
        btn_getStarted.setBackgroundColor(UICustomization.getBackgroundColor());
        btn_getStarted.setTextColor(UICustomization.getForegroundColor());

        btn_cancel = (Button) view.findViewById(R.id.activity_fourf_instructional_button_cancel);

        RelativeLayout background = (RelativeLayout) view.findViewById(R.id.activity_fourf_instructional_base_layout);
        background.setBackgroundColor(UICustomization.getDialogColor());

        tv_animationInfo = (TextView) view.findViewById(R.id.tv_animationInfo);
        tv_animationInfo.setTextColor(UICustomization.getForegroundColor());

        tv_failed = (TextView) view.findViewById(R.id.tv_failedLivenessHeading);
        tv_failed.setTextColor(UICustomization.getForegroundColor());

        iv_leftHand = (ImageView) view.findViewById(R.id.iv_animation_z7);
        iv_leftHand = (ImageView) view.findViewById(R.id.iv_animation_z7);
        iv_rightHand = (ImageView) view.findViewById(R.id.iv_animation_z8);
        iv_background = (ImageView) view.findViewById(R.id.iv_animation_z3);
        iv_phoneBackground = (ImageView) view.findViewById(R.id.iv_animation_z0);
        iv_phoneHand = (ImageView) view.findViewById(R.id.iv_animation_z1);
        iv_phoneGuide = (ImageView) view.findViewById(R.id.iv_animation_z4);
        iv_phoneGuideLiveness = (ImageView) view.findViewById(R.id.iv_animation_z4_liveness);
        iv_phoneROIs = (ImageView) view.findViewById(R.id.iv_animation_z5);
        iv_phoneColours = (ImageView) view.findViewById(R.id.iv_animation_z6);
        iv_flashOverlay = (ImageView) view.findViewById(R.id.iv_animation_z11);
        iv_phoneSuccess = (ImageView) view.findViewById(R.id.iv_animation_z12);

        iv_phoneROIs.setColorFilter(Color.RED, PorterDuff.Mode.MULTIPLY);

        iv_flashOverlay.setVisibility(View.INVISIBLE);
        iv_phoneSuccess.setVisibility(View.INVISIBLE);
        iv_phoneGuideLiveness.setVisibility(View.INVISIBLE);

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        phoneHeight = displayMetrics.heightPixels;
        phoneWidth = displayMetrics.widthPixels;




        if(livenessType == FourFInterface.LivenessType.STEREO_HORZ) {
            final ValueAnimator animator = ValueAnimator.ofFloat(-2, 6);
            animator.setDuration(8000);
            animator.setRepeatCount(ValueAnimator.INFINITE);
            animator.setRepeatMode(ValueAnimator.RESTART);
            animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(final ValueAnimator animation) {

                    float value = (Float) (animation.getAnimatedValue());
                    if(value < 0 ) {

                        if(completed){
                            completed = false;
                            iv_phoneSuccess.setVisibility(View.INVISIBLE);
                            iv_flashOverlay.setVisibility(View.INVISIBLE);
                            iv_phoneROIs.setColorFilter(Color.RED, PorterDuff.Mode.MULTIPLY);
                            iv_phoneGuideLiveness.setVisibility(View.INVISIBLE);
                            iv_phoneGuide.setVisibility(View.VISIBLE);
                            firstTime = true;
                        }

                        iv_leftHand.setTranslationX((phoneWidth / 24) * value);
                        iv_leftHand.setTranslationY(0);

                        iv_phoneHand.setTranslationX((phoneWidth / 18) * value);
                        iv_phoneHand.setTranslationY(0);


                        iv_phoneROIs.setTranslationX((phoneWidth / 18) * value);
                        iv_phoneROIs.setTranslationY(0);
                    }

                    else if (firstTime) {
                        firstTime = false;
                        iv_phoneROIs.setColorFilter(Color.GREEN, PorterDuff.Mode.MULTIPLY);
                        iv_flashOverlay.setVisibility(View.VISIBLE);
                        AnimationHelper.fadeInAndOutAnimation(getActivity(), iv_flashOverlay, ANIMATION_FLASH_DURATION, ANIMATION_FLASH_DURATION, ANIMATION_FLASH_TIME_BETWEEN);

                    }
                    else if (value > 1.5 && value <= 2){
                        iv_phoneSuccess.setVisibility(View.INVISIBLE);
                        iv_flashOverlay.setVisibility(View.INVISIBLE);
                        iv_phoneROIs.setColorFilter(Color.RED, PorterDuff.Mode.MULTIPLY);
                        iv_phoneGuide.setVisibility(View.INVISIBLE);
                        iv_phoneGuideLiveness.setVisibility(View.VISIBLE);
                    }
                    else if (value > 2 && value <= 3) {
                        iv_leftHand.setTranslationX((phoneWidth / 24) * (value - 2));
                        iv_leftHand.setTranslationY(0);

                        iv_phoneHand.setTranslationX((phoneWidth / 18) * (value - 2));
                        iv_phoneHand.setTranslationY(0);


                        iv_phoneROIs.setTranslationX((phoneWidth / 18) * (value - 2));
                        iv_phoneROIs.setTranslationY(0);
                    }
                    else if(value >3 && value <=4.5) {
                        iv_phoneROIs.setColorFilter(Color.GREEN, PorterDuff.Mode.MULTIPLY);
                        iv_flashOverlay.setVisibility(View.VISIBLE);
                        AnimationHelper.fadeInAndOutAnimation(getActivity(), iv_flashOverlay, ANIMATION_FLASH_DURATION, ANIMATION_FLASH_DURATION, ANIMATION_FLASH_TIME_BETWEEN);

                    }
                    else if (value > 5.5){
                        iv_phoneSuccess.setVisibility(View.VISIBLE);
                        completed = true;

                    }
                }
            });
            animator.start();
        }else if (livenessType == FourFInterface.LivenessType.STEREO_VERT){
            iv_phoneGuide.setImageDrawable(getResources().getDrawable(R.drawable.instructional_animation_phone_z4_up));
            iv_phoneGuideLiveness.setImageDrawable(getResources().getDrawable(R.drawable.instructional_animation_phone_z4_down));

            final ValueAnimator animator = ValueAnimator.ofFloat(-2, 6);
            animator.setDuration(8000);
            animator.setRepeatCount(ValueAnimator.INFINITE);
            animator.setRepeatMode(ValueAnimator.RESTART);
            animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(final ValueAnimator animation) {

                    float value = (Float) (animation.getAnimatedValue());
                    if(value < -1 ) {

                        if(completed){
                            completed = false;
                            iv_phoneSuccess.setVisibility(View.INVISIBLE);
                            iv_flashOverlay.setVisibility(View.INVISIBLE);
                            iv_phoneROIs.setColorFilter(Color.RED, PorterDuff.Mode.MULTIPLY);
                            iv_phoneGuideLiveness.setVisibility(View.INVISIBLE);
                            iv_phoneGuide.setVisibility(View.VISIBLE);
                            firstTime = true;
                        }

                        iv_leftHand.setScaleX(1.1f-(value+2)/18);
                        iv_leftHand.setScaleY(1.1f-(value+2)/18);
                        iv_leftHand.setTranslationX((phoneWidth/144)*(1-(value+2)));

                        iv_phoneHand.setTranslationX(0);
                        iv_phoneHand.setTranslationY((phoneHeight/37) * value);


                        iv_phoneROIs.setTranslationX(0);
                        iv_phoneROIs.setTranslationY((phoneHeight/37) * value);
                    }

                    else if (firstTime) {
                        firstTime = false;
                        iv_phoneROIs.setColorFilter(Color.GREEN, PorterDuff.Mode.MULTIPLY);
                        iv_flashOverlay.setVisibility(View.VISIBLE);
                        AnimationHelper.fadeInAndOutAnimation(getActivity(), iv_flashOverlay, ANIMATION_FLASH_DURATION, ANIMATION_FLASH_DURATION, ANIMATION_FLASH_TIME_BETWEEN);

                    }
                    else if (value > 0.5 && value <= 1){
                        iv_phoneSuccess.setVisibility(View.INVISIBLE);
                        iv_flashOverlay.setVisibility(View.INVISIBLE);
                        iv_phoneROIs.setColorFilter(Color.RED, PorterDuff.Mode.MULTIPLY);
                        iv_phoneGuide.setVisibility(View.INVISIBLE);
                        iv_phoneGuideLiveness.setVisibility(View.VISIBLE);
                    }
                    else if (value > 1 && value <= 4) {
                        iv_leftHand.setScaleX(1.1f-(value)/18);
                        iv_leftHand.setScaleY(1.1f-(value)/18);
                        iv_leftHand.setTranslationX((phoneWidth/144)*(1-(value)));

                        iv_phoneHand.setTranslationX(0);
                        iv_phoneHand.setTranslationY((phoneHeight/37) * (value-2));


                        iv_phoneROIs.setTranslationX(0);
                        iv_phoneROIs.setTranslationY((phoneHeight/37) * (value-2));
                    }
                    else if(value >4 && value <=4.5) {
                        iv_phoneROIs.setColorFilter(Color.GREEN, PorterDuff.Mode.MULTIPLY);
                        iv_flashOverlay.setVisibility(View.VISIBLE);
                        AnimationHelper.fadeInAndOutAnimation(getActivity(), iv_flashOverlay, ANIMATION_FLASH_DURATION, ANIMATION_FLASH_DURATION, ANIMATION_FLASH_TIME_BETWEEN);

                    }
                    else if (value > 5.5){
                        iv_phoneSuccess.setVisibility(View.VISIBLE);
                        completed = true;

                    }
                }
            });
            animator.start();
            /*final ValueAnimator animator = ValueAnimator.ofFloat(-2, 2);
            animator.setDuration(5000);
            animator.setRepeatCount(ValueAnimator.INFINITE);
            animator.setRepeatMode(ValueAnimator.RESTART);
            animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(final ValueAnimator animation) {

                    float value = ((Float) (animation.getAnimatedValue()))
                            .floatValue();

                    float topCornerX = iv_leftHand.getX();
                    float topCornerY = iv_leftHand.getY();
                    iv_leftHand.setScaleX(1.1f-(value+2)/18);
                    iv_leftHand.setScaleY(1.1f-(value+2)/18);
                    iv_leftHand.setTranslationX((phoneWidth/144)*(1-(value+2)));

                    iv_phoneHand.setTranslationX(0);
                    iv_phoneHand.setTranslationY((phoneHeight/37) * value);


                    iv_phoneROIs.setTranslationX(0);
                    iv_phoneROIs.setTranslationY((phoneHeight/37) * value);

                    if (value <= -0.9 && value >= -1.1 && firstTime) {
                        firstTime = false;
                        animation.pause();
                        iv_phoneROIs.setColorFilter(Color.GREEN, PorterDuff.Mode.MULTIPLY);
                        iv_flashOverlay.setVisibility(View.VISIBLE);
                        AnimationHelper.fadeInAndOutAnimation(getActivity(), iv_flashOverlay, ANIMATION_FLASH_DURATION, ANIMATION_FLASH_DURATION, ANIMATION_FLASH_TIME_BETWEEN);


                        final Handler handler2 = new Handler();
                        handler2.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                iv_phoneSuccess.setVisibility(View.INVISIBLE);
                                iv_flashOverlay.setVisibility(View.INVISIBLE);
                                iv_phoneROIs.setColorFilter(Color.RED, PorterDuff.Mode.MULTIPLY);
                                iv_phoneGuide.setVisibility(View.INVISIBLE);
                                iv_phoneGuideLiveness.setVisibility(View.VISIBLE);
                                animation.resume();
                            }
                        }, 1500);
                    }
                    if (value <= 2 && value > 1.9) {
                        animation.pause();
                        iv_phoneROIs.setColorFilter(Color.GREEN, PorterDuff.Mode.MULTIPLY);
                        iv_flashOverlay.setVisibility(View.VISIBLE);
                        AnimationHelper.fadeInAndOutAnimation(getActivity(), iv_flashOverlay, ANIMATION_FLASH_DURATION, ANIMATION_FLASH_DURATION, ANIMATION_FLASH_TIME_BETWEEN);

                        final Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                iv_phoneSuccess.setVisibility(View.VISIBLE);
                            }
                        }, 750);

                        final Handler handler2 = new Handler();
                        handler2.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                iv_phoneSuccess.setVisibility(View.INVISIBLE);
                                iv_flashOverlay.setVisibility(View.INVISIBLE);
                                iv_phoneROIs.setColorFilter(Color.RED, PorterDuff.Mode.MULTIPLY);
                                iv_phoneGuideLiveness.setVisibility(View.INVISIBLE);
                                iv_phoneGuide.setVisibility(View.VISIBLE);
                                animation.start();
                            }
                        }, 1500);
                        firstTime = true;

                    }
                }
            });
            animator.start();*/
        }
        ((DefaultFourFBiometricsActivity) baseActivity).onLivenessInstructionalFragmentReady(this);
    }
}

