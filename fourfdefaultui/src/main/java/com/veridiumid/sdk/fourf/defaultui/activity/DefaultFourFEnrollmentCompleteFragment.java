package com.veridiumid.sdk.fourf.defaultui.activity;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.veridiumid.sdk.fourf.defaultui.R;
import com.veridiumid.sdk.support.base.VeridiumBaseFragment;

public class DefaultFourFEnrollmentCompleteFragment extends VeridiumBaseFragment {

    protected Button btn_continue;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.layout_4f_complete, container, false);
    }

    @Override
    protected void initView(View view) {
        super.initView(view);

        View base = (View) view.findViewById(R.id.activity_fourf_complete_base_layout);
        btn_continue = (Button) view.findViewById(R.id.layout_4f_complete_btn_continue);
        TextView tv_heading = (TextView) view.findViewById(R.id.activity_fourf_complete_tv_heading);
        TextView tv_title = (TextView) view.findViewById(R.id.activity_fourf_complete_tv_title);

        base.setBackgroundColor(UICustomization.getBackgroundColor());
        btn_continue.setTextColor(UICustomization.getForegroundColor());
        tv_heading.setTextColor(UICustomization.getForegroundColor());
        tv_title.setTextColor(UICustomization.getForegroundColor());

        DefaultFourFBiometricsActivity parentActivity = ((DefaultFourFBiometricsActivity) baseActivity);
        parentActivity.onEnrollmentCompleteReady(this);
    }
}
