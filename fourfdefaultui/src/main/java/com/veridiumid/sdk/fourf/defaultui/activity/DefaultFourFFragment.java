package com.veridiumid.sdk.fourf.defaultui.activity;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.veridiumid.sdk.fourf.FourFInterface;
import com.veridiumid.sdk.fourf.defaultui.R;
import com.veridiumid.sdk.support.base.VeridiumBaseFragment;
import com.veridiumid.sdk.support.ui.AspectRatioSafeFrameLayout;

public class  DefaultFourFFragment extends VeridiumBaseFragment {

    private static final String LOG_TAG = DefaultFourFFragment.class.getName();

    private DefaultFourFBiometricsActivity mFourFActivity = null;

    protected ImageView iv_cancelCircle;
    protected ImageView iv_cancelIcon;
    protected RelativeLayout rl_cancel;

    protected TextView tv_info;
    protected TextView tv_countDown;
    protected TextView tv_countDownLeft;
    protected TextView tv_placeYourFingers;


    protected TextView tv_switchHands;

    protected RelativeLayout rl_top;
    //protected RelativeLayout rl_countDown;
    //protected RelativeLayout rl_countDownLeft;
    protected RelativeLayout rl_centre_message;

    //protected ImageView iv_countDown;
    //protected ImageView iv_countDownLeft;

    protected RealtimeRoisDecorator roiRenderer;
    protected AspectRatioSafeFrameLayout mCameraLayout;
    protected ImageView iv_imgFingerHint;

    ImageView iv_imgGuidanceNone; // An empty/invisible image for when nothing should be shown. This allows the rule that one is always shown.
    ImageView[] array_GuidanceImages = new ImageView[FourFInterface.TrackingState.nStates]; // Array allows indexing to the relevant
    int int_currentlyShownGuidanceImageIndex = 0;  // ImageViews for toggling which is shown.

    protected TextView tv_switchLeftHand;
    protected TextView tv_switchRightHand;

    protected ImageView iv_switchHandCircle;
    protected ImageView iv_switchHandIcon;
    protected RelativeLayout rl_switchHand;

    protected boolean handGuideWasSet = false;

    private int i = 1;
    public TextView tv_moveDots;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(com.veridiumid.sdk.fourf.defaultui.R.layout.layout_fourf_security, container, false);
    }

    @Override
    protected void initView(View view) {

        super.initView(view);

        Drawable logo = UICustomization.getLogo();
        if (logo != null) {
            ImageView logoBox = (ImageView) view.findViewById(R.id.iv_consumer_logo);
            logoBox.setImageDrawable(logo);
        }

        mCameraLayout = (AspectRatioSafeFrameLayout) view.findViewById(R.id.camera_preview);

        mFourFActivity = (DefaultFourFBiometricsActivity) baseActivity;

        rl_cancel = (RelativeLayout) view.findViewById((R.id.rl_cancel));
        iv_cancelCircle = (ImageView) view.findViewById(R.id.cancel_circle);
        UICustomization.applyBackgroundColorMask(iv_cancelCircle);
        iv_cancelIcon = (ImageView) view.findViewById(R.id.cancel_icon);
        UICustomization.applyForegroundColorMask(iv_cancelIcon);
        //tv_cancel.setTextColor(UICustomization.getForegroundColor());

        //tv_info = (TextView) view.findViewById(com.veridiumid.sdk.fourf.defaultui.R.id.tv_info);
        //tv_countDown = (TextView) view.findViewById(com.veridiumid.sdk.fourf.defaultui.R.id.tv_countDown);
        //tv_countDownLeft = (TextView) view.findViewById(com.veridiumid.sdk.fourf.defaultui.R.id.tv_countDownLeft);
        //tv_countDown.setTextColor(UICustomization.getForegroundColor());
        //tv_countDownLeft.setTextColor(UICustomization.getForegroundColor());



        tv_placeYourFingers = (TextView) view.findViewById(R.id.tv_placeYourFingers);
        tv_placeYourFingers.setTextColor(UICustomization.getForegroundColor());

        //rl_countDown = (RelativeLayout) view.findViewById(com.veridiumid.sdk.fourf.defaultui.R.id.rl_countDown);
        //rl_countDownLeft = (RelativeLayout) view.findViewById(com.veridiumid.sdk.fourf.defaultui.R.id.rl_countDownLeft);
        rl_centre_message = (RelativeLayout) view.findViewById(R.id.rl_centre_message);

        tv_moveDots = (TextView) view.findViewById(R.id.tv_dots);
        tv_moveDots.setTextColor(UICustomization.getForegroundColor());

        //iv_countDown = (ImageView) view.findViewById(R.id.iv_countDown);
        //iv_countDownLeft = (ImageView) view.findViewById(R.id.iv_countDownLeft);
        //iv_countDown.setImageDrawable(UICustomization.getImageWithBackgroundColor(getResources().getDrawable(R.drawable.countdown_circle)));
        //iv_countDownLeft.setImageDrawable(UICustomization.getImageWithBackgroundColor(getResources().getDrawable(R.drawable.countdown_circle)));

        iv_imgFingerHint = (ImageView) view.findViewById(R.id.img_finger_hint);
        //tv_switchLeftHand = (TextView) view.findViewById(com.veridiumid.sdk.fourf.defaultui.R.id.tv_switchLeftHand);
        //tv_switchRightHand = (TextView) view.findViewById(com.veridiumid.sdk.fourf.defaultui.R.id.tv_switchRightHand);
        //iv_switchLeftHand = (ImageView) view.findViewById(com.veridiumid.sdk.fourf.defaultui.R.id.iv_switchLeftHand);
        //iv_switchRightHand = (ImageView) view.findViewById(com.veridiumid.sdk.fourf.defaultui.R.id.iv_switchRightHand);
        iv_switchHandCircle = (ImageView) view.findViewById(R.id.switch_hand_circle);
        UICustomization.applyBackgroundColorMask(iv_switchHandCircle);
        iv_switchHandIcon = (ImageView) view.findViewById(R.id.switch_hand_icon);
        UICustomization.applyForegroundColorMask(iv_switchHandIcon);
        rl_switchHand = (RelativeLayout) view.findViewById(R.id.rl_switch_hand);

        ImageView iv_leftArrow = (ImageView) view.findViewById(R.id.iv_guidance_leftArrow);
        ImageView iv_rightArrow = (ImageView) view.findViewById(R.id.iv_guidance_rightArrow);
        ImageView iv_backwardArrow = (ImageView) view.findViewById(R.id.iv_guidance_backwardArrow);
        ImageView iv_forwardArrow = (ImageView) view.findViewById(R.id.iv_guidance_forwardArrow);
        ImageView iv_downArrow = (ImageView) view.findViewById(R.id.iv_guidance_downArrow);
        ImageView iv_upArrow = (ImageView) view.findViewById(R.id.iv_guidance_upArrow);

        UICustomization.applyBackgroundColorMask(iv_forwardArrow);
        UICustomization.applyBackgroundColorMask(iv_backwardArrow);
        UICustomization.applyBackgroundColorMask(iv_leftArrow);
        UICustomization.applyBackgroundColorMask(iv_rightArrow);
        UICustomization.applyBackgroundColorMask(iv_upArrow);
        UICustomization.applyBackgroundColorMask(iv_downArrow);

        UICustomization.applyFingerColorMask(iv_imgFingerHint);


        //Define guidance images and put them in the array; indexing must match FourFImagingProcessor.
        iv_imgGuidanceNone = (ImageView) view.findViewById(R.id.iv_guidance_none);
        array_GuidanceImages[FourFInterface.TrackingState.PREVIEW_STAGE_NORMAL.getCode()] = iv_imgGuidanceNone;
        array_GuidanceImages[FourFInterface.TrackingState.PREVIEW_STAGE_NULL.getCode()] = iv_imgGuidanceNone;
        array_GuidanceImages[FourFInterface.TrackingState.PREVIEW_STAGE_STABILIZED.getCode()] = iv_imgGuidanceNone;
        array_GuidanceImages[FourFInterface.TrackingState.PREVIEW_STAGE_TOO_CLOSE.getCode()] = iv_backwardArrow;
        array_GuidanceImages[FourFInterface.TrackingState.PREVIEW_STAGE_TOO_FAR.getCode()] = iv_forwardArrow;
        array_GuidanceImages[FourFInterface.TrackingState.PREVIEW_STAGE_FINGERS_APART.getCode()] = iv_imgGuidanceNone;
        array_GuidanceImages[FourFInterface.TrackingState.PREVIEW_STAGE_TOO_HIGH.getCode()] = iv_downArrow;
        array_GuidanceImages[FourFInterface.TrackingState.PREVIEW_STAGE_TOO_LOW.getCode()] = iv_upArrow;
        array_GuidanceImages[FourFInterface.TrackingState.PREVIEW_STAGE_TOO_LEFT.getCode()] = iv_rightArrow;
        array_GuidanceImages[FourFInterface.TrackingState.PREVIEW_STAGE_TOO_RIGHT.getCode()] = iv_leftArrow;
        array_GuidanceImages[FourFInterface.TrackingState.PREVIEW_STAGE_FRAME_DIM.getCode()] = iv_imgGuidanceNone;
        array_GuidanceImages[FourFInterface.TrackingState.PREVIEW_STAGE_PICTURE_REQUESTED.getCode()] = iv_imgGuidanceNone;
        array_GuidanceImages[FourFInterface.TrackingState.PREVIEW_STAGE_TAKING_PICTURE.getCode()] = iv_imgGuidanceNone;
        array_GuidanceImages[FourFInterface.TrackingState.PREVIEW_STAGE_NOT_CENTERED.getCode()] = iv_imgGuidanceNone;
        for (int makeInvisibleIndex = 0; makeInvisibleIndex < FourFInterface.TrackingState.nStates; makeInvisibleIndex++){
            array_GuidanceImages[makeInvisibleIndex].setVisibility(View.INVISIBLE);
        }


        rl_top = (RelativeLayout) view.findViewById(R.id.rl_top);
        rl_top.setBackgroundColor(UICustomization.getBackgroundColor());

        mFourFActivity.onFourFFragmentReady(this);
    }

    public void setGuidanceSymbol(FourFInterface.TrackingState newGuidanceIndex) {
        if (int_currentlyShownGuidanceImageIndex != newGuidanceIndex.getCode()) {
            array_GuidanceImages[int_currentlyShownGuidanceImageIndex].setVisibility(View.INVISIBLE);
            array_GuidanceImages[newGuidanceIndex.getCode()].setVisibility(View.VISIBLE);
            int_currentlyShownGuidanceImageIndex = newGuidanceIndex.getCode();
        }
    }

}
