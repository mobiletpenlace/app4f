package com.veridiumid.sdk.fourf.defaultui.activity;

import android.animation.ValueAnimator;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.veridiumid.sdk.fourf.defaultui.R;
import com.veridiumid.sdk.imaging.AnimationHelper;
import com.veridiumid.sdk.support.base.VeridiumBaseFragment;

/**
 * Created by tyson on 30/11/2016.
 */

public class DefaultFourFCaptureInstructionalThumbFragment extends VeridiumBaseFragment {

    protected Button btn_getStarted;
    protected Button btn_cancel;

    protected ImageView texture;

    protected TextView tv_animationInfo;

    protected ImageView iv_leftHand;
    protected ImageView iv_rightHand;
    protected ImageView iv_background;
    protected ImageView iv_phoneBackground;
    protected ImageView iv_phoneHand;
    protected ImageView iv_phoneGuide;
    protected ImageView iv_phoneColours;
    protected ImageView iv_flashOverlay;
    protected ImageView iv_phoneSuccess;
    protected ImageView iv_phoneROIs;



    private int leftAnimationCount = 0;
    private int rightAnimationCount = 0;

    private final static int ANIMATION_DURATION = 1000;
    private final static int ANIMATION_DELAY = 1000;
    private final static int ANIMATION_RESTART_DELAY = 2000;
    private final static int ANIMATION_FLASH_DURATION = 200;
    private final static int ANIMATION_FLASH_TIME_BETWEEN = 50;

    private int phoneHeight;
    private int phoneWidth;
    private ValueAnimator animator;

    private boolean hasJustStartedFirstStage = false;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.layout_4f_instructional_thumb_animation, container, false);
    }

    @Override
    protected void initView(final View view) {
        super.initView(view);
        btn_getStarted = (Button) view.findViewById(R.id.activity_fourf_instructional_button_begin);
        btn_getStarted.setBackgroundColor(UICustomization.getBackgroundColor());
        btn_getStarted.setTextColor(UICustomization.getForegroundColor());
        btn_cancel = (Button) view.findViewById(R.id.activity_fourf_instructional_button_cancel);
        tv_animationInfo = (TextView) view.findViewById(R.id.tv_animationInfo);
        tv_animationInfo.setTextColor(UICustomization.getForegroundColor());
        iv_leftHand = (ImageView) view.findViewById(R.id.iv_animation_z7);
        iv_leftHand = (ImageView) view.findViewById(R.id.iv_animation_z7);
        iv_rightHand = (ImageView) view.findViewById(R.id.iv_animation_z8);
        iv_background = (ImageView) view.findViewById(R.id.iv_animation_z3);
        iv_phoneBackground = (ImageView) view.findViewById(R.id.iv_animation_z0);
        iv_phoneHand = (ImageView) view.findViewById(R.id.iv_animation_z1);
        iv_phoneGuide = (ImageView) view.findViewById(R.id.iv_animation_z4);
        iv_phoneColours = (ImageView) view.findViewById(R.id.iv_animation_z6);
        iv_flashOverlay = (ImageView) view.findViewById(R.id.iv_animation_z11);
        iv_phoneSuccess = (ImageView) view.findViewById(R.id.iv_animation_z12);
        iv_phoneROIs = (ImageView) view.findViewById(R.id.iv_animation_z5);

        iv_phoneROIs.setColorFilter(Color.RED, PorterDuff.Mode.MULTIPLY);
        tv_animationInfo.setText(getString(R.string.thumb_instructional));
        texture = (ImageView) view.findViewById(R.id.imageView3);
        UICustomization.applyBackgroundColorMask(texture);
        iv_flashOverlay.setVisibility(View.INVISIBLE);
        iv_phoneSuccess.setVisibility(View.INVISIBLE);

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        phoneHeight = displayMetrics.heightPixels;
        phoneWidth = displayMetrics.widthPixels;


        animator = ValueAnimator.ofFloat(0, 2.8f);
        animator.setDuration(7000);
        animator.setRepeatMode(ValueAnimator.RESTART);
        animator.setRepeatCount(ValueAnimator.INFINITE);
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener()

        {
            @Override
            public void onAnimationUpdate(final ValueAnimator animation) {

                float value = ((Float) (animation.getAnimatedValue()))
                        .floatValue();
                if(value < 2) {
                    if (!hasJustStartedFirstStage) {
                        hasJustStartedFirstStage = true;

                        iv_phoneSuccess.setVisibility(View.INVISIBLE);
                        iv_flashOverlay.setVisibility(View.INVISIBLE);

                        iv_phoneROIs.setColorFilter(Color.RED, PorterDuff.Mode.MULTIPLY);


                    }

                    iv_leftHand.setTranslationX((float) ((phoneWidth / 24) * (2 - value) * Math.sin(value * Math.PI)));
                    iv_leftHand.setTranslationY((float) ((3 * phoneHeight / 128) * (2 - value) * Math.cos(value * Math.PI)));


                    float width2 = iv_phoneHand.getMeasuredWidth();
                    float height2 = iv_phoneHand.getMeasuredHeight();
                    iv_phoneHand.setPivotX((613f/720f)*width2);
                    iv_phoneHand.setPivotY((333f/452f)*height2);
                    iv_phoneHand.setTranslationX((float) ((5 * phoneWidth / 96) * (2 - value) * Math.sin(value * Math.PI)));
                    iv_phoneHand.setTranslationY((float) ((3 * phoneHeight / 512) * Math.cos(2 * value * Math.PI)));
                    iv_phoneHand.setScaleX(((float) ((2 - value) * Math.cos(value * Math.PI)) / 4) + 1);
                    iv_phoneHand.setScaleY(((float) ((2 - value) * Math.cos(value * Math.PI)) / 4) + 1);


                }

                else if (hasJustStartedFirstStage){

                    hasJustStartedFirstStage = false;
                    iv_phoneROIs.setColorFilter(Color.GREEN, PorterDuff.Mode.MULTIPLY);
                    iv_flashOverlay.setVisibility(View.VISIBLE);
                    AnimationHelper.fadeInAndOutAnimation(getActivity(), iv_flashOverlay, ANIMATION_FLASH_DURATION, ANIMATION_FLASH_DURATION, ANIMATION_FLASH_TIME_BETWEEN);

                    final Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            iv_phoneSuccess.setVisibility(View.VISIBLE);
                        }
                    }, 1000);
                }
            }
        });

        animator.start();
        ((DefaultFourFBiometricsActivity) baseActivity).onInstructionalThumbFragmentReady(this);
    }

    @Override
    public void onDestroyView(){
        super.onDestroyView();
        animator.end();

    }
}
