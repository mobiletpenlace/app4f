package com.veridiumid.sdk.fourf.defaultui.activity;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.RectF;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Vibrator;
import android.support.annotation.UiThread;
import android.support.v7.app.ActionBar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.veridiumid.sdk.IBiometricFormats.TemplateFormat;
import com.veridiumid.sdk.analytics.Analytics;
import com.veridiumid.sdk.core.biometrics.engine.IBiometricsEngine;
import com.veridiumid.sdk.core.biometrics.engine.domain.BiometricsResult;
import com.veridiumid.sdk.core.biometrics.engine.handling.AdaptiveEnrollmentHandler;
import com.veridiumid.sdk.core.biometrics.engine.handling.AuthenticationHandler;
import com.veridiumid.sdk.core.biometrics.engine.handling.ChainedHandler;
import com.veridiumid.sdk.core.biometrics.engine.handling.PersistEnrollmentHandler;
import com.veridiumid.sdk.core.biometrics.engine.handling.matching.LocalBiometricMatcher;
import com.veridiumid.sdk.core.biometrics.engine.impl.DecentralizedBiometricsEngineImpl;
import com.veridiumid.sdk.core.biometrics.persistence.impl.BytesTemplatesStorage;
import com.veridiumid.sdk.core.biometrics.persistence.impl.TemplateProviderCache;
import com.veridiumid.sdk.core.biometrics.persistence.impl.TemplateProviderFromTemplateStorage;
import com.veridiumid.sdk.core.biometrics.persistence.impl.TemplateStorageInMemory;
import com.veridiumid.sdk.core.data.persistence.IKVStore;
import com.veridiumid.sdk.core.data.persistence.impl.InMemoryKVStore;
import com.veridiumid.sdk.fourf.CaptureConfig;
import com.veridiumid.sdk.fourf.FourFInterface;
import com.veridiumid.sdk.fourf.FourFInterface.LivenessType;
import com.veridiumid.sdk.fourf.FourFInterface.OptimiseMode;
import com.veridiumid.sdk.fourf.defaultui.R;
import com.veridiumid.sdk.fourfintegration.AdaptiveEnrollmentHandlerLive;
import com.veridiumid.sdk.fourfintegration.ExportConfig;
import com.veridiumid.sdk.fourfintegration.FourFIntegrationWrapper;
import com.veridiumid.sdk.fourfintegration.FourFProcessor;
import com.veridiumid.sdk.fourfintegration.HandGuideHelper;
import com.veridiumid.sdk.fourfintegration.IFourFProcessingListener;
import com.veridiumid.sdk.fourfintegration.IFourFTrackingListener;
import com.veridiumid.sdk.fourfintegration.LocalFourFBiometricMatcher;
import com.veridiumid.sdk.fourfintegration.LocalFourFGetLivenessTemplateRH;
import com.veridiumid.sdk.fourfintegration.LocalFourFGetSecondLivenessTemplateRH;
import com.veridiumid.sdk.fourfintegration.LocalFourFGetTemplateInternalRH;
import com.veridiumid.sdk.fourfintegration.ThumbProcessor;
import com.veridiumid.sdk.imaging.CameraLayoutDecorator;
import com.veridiumid.sdk.imaging.CameraSamplingPolicy;
import com.veridiumid.sdk.imaging.DefaultFourFCameraPolicy;
import com.veridiumid.sdk.imaging.help.CameraHelper;
import com.veridiumid.sdk.imaging.help.DisplayHelper;
import com.veridiumid.sdk.imaging.model.ImageHolder;
import com.veridiumid.sdk.support.BaseImagingBiometricsActivity;
import com.veridiumid.sdk.support.help.CustomCountDownTimer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.veridiumid.sdk.fourf.defaultui.R.string.place_fingers_in_template;
import static java.lang.Math.abs;

public class DefaultFourFBiometricsActivity extends BaseImagingBiometricsActivity implements IFourFTrackingListener, IFourFProcessingListener {

    private final int ENROLLMENT_STEP1_COMPLETE = 1;
    private final int INDEX_COMPLETE = 2;
    private final int SWITCH_HANDS = 3;
    private final int ENROLLMENT_COMPLETE = 4;
    private final int AUTHENTICATION_COMPLETE = 5;
    private final int FAILED_SCAN = 6;
    private final int CAPTURE_COMPLETE = 7;
    private final int PASSIVE_LIVENESS_FAILED = 8;

    private static final String LOG_TAG = DefaultFourFBiometricsActivity.class.getName();

    private static final int ADAPTIVE_TEMPLATES_LIMIT = 3;

    private static boolean AUTO_START_DEFAULT = true;

    private static boolean async = true;

    protected FourFProcessor fourFProcessor;

    protected ThumbProcessor thumbProcessor;

    protected CustomCountDownTimer mCountDownTimer;

    protected CustomCountDownTimer mSwitchHandTimer; // time how long we show intermediate fragments

    private int SWITCH_HAND_TIMEOUT = 2000;

    private int FOURF_TIMEOUT = 120000;

    private int FOURF_TIMEOUT_WARN = 20000;

    private boolean timer_state = false;

    private final int SECOND = 1000;

    private int currentConfigIndex; // counter through the list

    private List<CaptureConfig> captureSequence = new ArrayList<>(); // list of configs to cycle through for capture

    private CaptureConfig currentConfig; // current configuration

    protected Map<String, BiometricsResult<ImageHolder>> finalResults; // sent off in super.onComplete()

    // Stores all results prior to sending off to super.onComplete()
    private BiometricsResult<ImageHolder> previousResults;
    private BiometricsResult<ImageHolder> eightF_fuse_queue; // when ==2, gets joined and added to previousResults
    private BiometricsResult<ImageHolder> multishot_fuse_queue; // when ==2, gets merged and added to previousResults
    private List<Integer> outputSlots;
    private BiometricsResult<ImageHolder> outputResults;

    private BytesTemplatesStorage templates_store_left; // enroll templates
    private BytesTemplatesStorage templates_store_right;

    private BytesTemplatesStorage templates_store; // store to pass to enrol handler

    protected void setTimeout(int timeout) {
        FOURF_TIMEOUT = timeout;
    }

    private void setupActionbarAndStatusbar() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            if (getSupportActionBar() != null) {
                ActionBar actionBar;
                actionBar = getSupportActionBar();
                actionBar.setIcon(new ColorDrawable(this.getResources().getColor(android.R.color.transparent)));
                actionBar.setBackgroundDrawable(new ColorDrawable(this.getResources().getColor(R.color.colorPrimaryDark)));
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                Window window = this.getWindow();
                window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.setStatusBarColor(this.getResources().getColor(R.color.colorAccent));
            }
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        //getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);

        if (getSupportActionBar() != null) {
            getSupportActionBar().hide();
        }

        setupActionbarAndStatusbar();
        setContentView(com.veridiumid.sdk.fourf.defaultui.R.layout.activity_fragment_main);
        persistence = openStorage();
        openTemplateStorage();

        setUpInitialResults();

        if (!checkFormatSupported()) {  // Check that the chosen formats are supported
            onError(0, getString(R.string.unsupported_template_format) + ":" + TemplateFormat.resolveFriendly(ExportConfig.getFormat()));
        }

        if (isEnrollment()) {
            clearEnrollement();
            setup_enrollSequence();
            if (tipsDisabled()) {
                kickOffBiometricsProcess();
            } else {
                showFragment(new DefaultFourFCaptureInstructionalFragment());
            }
        } else if (isEnrollExport()) {
            clearEnrollement();
            setup_enrollExportSequence();
            if (tipsDisabled()) {
                kickOffBiometricsProcess();
            } else {
                showFragment(new DefaultFourFCaptureInstructionalFragment());
            }
        } else if (isCapture()) {
            setup_captureSequence();
            if (tipsDisabled()) {
                kickOffBiometricsProcess();
            } else {
                showFragment(new DefaultFourFCaptureInstructionalFragment());
            }
        } else if (isCapture8F()) {
            setup_8F_captureSequence();
            if (tipsDisabled()) {
                kickOffBiometricsProcess();
            } else {
                showFragment(new DefaultFourFCaptureInstructionalFragment());
            }
        } else if (isCapture2THUMB()) {
            setup_2F_capture_basic_Sequence();
            if (tipsDisabled()) {
                kickOffBiometricsProcess();
            } else {
                showFragment(new DefaultFourFCaptureInstructionalThumbFragment());
            }
        } else if (isCaptureTHUMB()) {
            setup_1F_capture_basic_Sequence();
            if (tipsDisabled()) {
                kickOffBiometricsProcess();
            } else {
                showFragment(new DefaultFourFCaptureInstructionalThumbFragment());
            }
        } else if (isAuthentication()) {
            if (!isEnrolled()) {
                onError(0, "User not enrolled");
                return;
            }
            setup_authenticationSequence();
            kickOffBiometricsProcess();
        } else {
            onError(0, "Unknown Mode");
        }
    }

    private void setUpInitialResults() {
        currentConfigIndex = 0;
        previousResults = new BiometricsResult<ImageHolder>(FourFInterface.UID);
        outputResults = new BiometricsResult<ImageHolder>(FourFInterface.UID);
        eightF_fuse_queue = new BiometricsResult<ImageHolder>(FourFInterface.UID);
        multishot_fuse_queue = new BiometricsResult<ImageHolder>(FourFInterface.UID);
    }

    /* Check if a user is enrolled with at least one hand
     * Templates should have been opened with openTemplateStorage();
     * Can be enrolled for left, right, or both.
     */
    private boolean isEnrolled() {
        boolean enrolled = false;
        if (templates_store_left != null) {
            enrolled = templates_store_left.isEnrolled();
        }
        if (templates_store_right != null) {
            enrolled = enrolled || templates_store_right.isEnrolled();
        }
        return enrolled;
    }

    /*
     Create a capture sequence for single hand enrolment
     */

    private void setup_enrollSequence() {
        ExportConfig.setFormat(TemplateFormat.FORMAT_VERIDFP); // output is always VERIDFP
        boolean rightHand = ExportConfig.getCaptureHandSide() == ExportConfig.CaptureHand.RIGHT;

        FourFInterface.LivenessType livenessType = null;
        if (ExportConfig.getUseLiveness()) {
            livenessType = LivenessType.STEREO_HORZ;
        } else {
            livenessType = FourFInterface.LivenessType.NONE;
        }

        CaptureConfig firstImage = new CaptureConfig(FourFInterface.CaptureType.ENROLMENT_ONE,
                TemplateFormat.FORMAT_VERIDFP, rightHand, livenessType);
        firstImage.setStoreForMatch(true);
        firstImage.setAllowUserHandSwitch(!ExportConfig.getCaptureHandFixed());
        firstImage.setStoreAsEnrolTemplate(true);
        firstImage.setShowProcessingScreen(true);

        livenessType = FourFInterface.LivenessType.NONE;
        CaptureConfig secondImage = new CaptureConfig(FourFInterface.CaptureType.ENROLMENT_TWO,
                TemplateFormat.FORMAT_VERIDFP, rightHand, livenessType);
        secondImage.setMatchWithStored(true);
        secondImage.setUserSelectedHand(!ExportConfig.getCaptureHandFixed());
        secondImage.setStoreAsEnrolTemplate(true);
        secondImage.setShowProcessingScreen(true);

        captureSequence.clear();
        captureSequence.add(firstImage);
        captureSequence.add(secondImage);

        outputSlots = Arrays.asList(0);

        Log.d(LOG_TAG, ExportConfig.getConfig());

        Analytics.sendAnalyticsEvents(Analytics.verbosity.DEBUG, Analytics.Cat.EXPORT_CONF, ExportConfig.getConfig());
        Analytics.sendAnalyticsEvents(Analytics.verbosity.DEBUG, Analytics.Cat.SEQUENCE, "Enroll Sequence");

    }

    private void setup_enrollExportSequence() {
        ExportConfig.setFormat(TemplateFormat.FORMAT_JSON); // output is always JSON
        ExportConfig.setLaxLiveness(false);
        ExportConfig.setUseLiveness(false);
        LivenessType livenessType;
        CaptureConfig.JoinMergeFormat = TemplateFormat.FORMAT_JSON;

        // hand selection
        boolean rightHand = false;
        boolean allowHandSwitch = false;
        if (ExportConfig.getCaptureHandFixed()) {
            if (ExportConfig.getCaptureHandSide() == ExportConfig.CaptureHand.RIGHT) {
                rightHand = true;
            }
        } else {
            allowHandSwitch = true;
        }

        //---------
        livenessType = LivenessType.STEREO_VERT;
        CaptureConfig firstImage = new CaptureConfig(FourFInterface.CaptureType.NONE,
                TemplateFormat.FORMAT_VERIDFP, rightHand, livenessType);

        firstImage.setAllowUserHandSwitch(allowHandSwitch);

        firstImage.setStoreForMatch(true);        // store and save first liveness image for match and enrol
        firstImage.setStoreAsEnrolTemplate(true);

        firstImage.setSecondLivenessImageAsEnrol(true); // match second livness image to stored, and then save as an enrol

        firstImage.setGetTemplateFromLivenessImage(true); // get additional templates (in non VFP formats) for export
        firstImage.setGetTemplateFromSecondLivenessImage(true);
        firstImage.setAdditionalTemplateformat(TemplateFormat.FORMAT_JSON);

        firstImage.setStoreTemplate_to_multishot_queue(true); // send to fuse que to get a single output
        firstImage.setSecondLiveness_to_multishot_queue(true);

        firstImage.setShowProcessingScreen(true);

        captureSequence.clear();
        captureSequence.add(firstImage);

        outputSlots = Arrays.asList(2); // final 8F, multi-shot merged result
        Analytics.sendAnalyticsEvents(Analytics.verbosity.DEBUG, Analytics.Cat.EXPORT_CONF, ExportConfig.getConfig());
        Analytics.sendAnalyticsEvents(Analytics.verbosity.DEBUG, Analytics.Cat.SEQUENCE, "Enroll-Export Sequence");
    }

    /*
     *   Create a capture sequence for single hand authentication
    */
    private void setup_authenticationSequence() {
        ExportConfig.setFormat(TemplateFormat.FORMAT_VERIDFP); // output is always VERIDFP
        ExportConfig.setLaxLiveness(false);
        ExportConfig.setUseLiveness(false);
        boolean rightHand = false;
        boolean allowHandSwitch = false;

        // Determine which hand to auth
        if (ExportConfig.getCaptureHandFixed()) {
            if (ExportConfig.getCaptureHandSide() == ExportConfig.CaptureHand.RIGHT && templates_store_right.isEnrolled()) {
                rightHand = true;
            } else if (ExportConfig.getCaptureHandSide() == ExportConfig.CaptureHand.LEFT && templates_store_left.isEnrolled()) {
                rightHand = false;
            } else {
                onError(0, "Forced request hand is not enrolled");
            }
        } else {
            if ((ExportConfig.getCaptureHandSide() == ExportConfig.CaptureHand.RIGHT && templates_store_right.isEnrolled())
                    || !templates_store_left.isEnrolled()) {
                rightHand = true;
            } else {
                rightHand = false;
            }
            if (templates_store_right.isEnrolled() && templates_store_left.isEnrolled()) {
                allowHandSwitch = true;
            }
        }

        FourFInterface.CaptureType type = FourFInterface.CaptureType.AUTH;
        LivenessType livenessType = null;
        if (ExportConfig.getUseLiveness()) {
            livenessType = LivenessType.STEREO_HORZ;
        } else {
            livenessType = LivenessType.NONE;
        }

        CaptureConfig firstImage = new CaptureConfig(type, TemplateFormat.FORMAT_VERIDFP, rightHand, livenessType);
        firstImage.setStoreAsEnrolTemplate(true); // for adaptive enrollment
        firstImage.setMatchAgainstEnrol(true);
        firstImage.setAllowUserHandSwitch(allowHandSwitch);
        firstImage.setShowProcessingScreen(true);

        captureSequence.clear();
        captureSequence.add(firstImage);

        outputSlots = Arrays.asList(0);

        Analytics.sendAnalyticsEvents(Analytics.verbosity.DEBUG, Analytics.Cat.EXPORT_CONF, ExportConfig.getConfig());
        Analytics.sendAnalyticsEvents(Analytics.verbosity.DEBUG, Analytics.Cat.SEQUENCE, "Authentification Sequence");
    }


    /*
     Create a capture sequence for extracting export templates from both hands
     Supports various combo's of liveness, and finger optimisations
     */
    private void setup_8F_captureSequence() {
        if (ExportConfig.getOptimiseForIndexLittle()) {
            if (ExportConfig.getUseLiveness()) {
                setup_8F_capture_DoubleOptimise_Liveness_Sequence();
            } else {
                setup_8F_capture_DoubleOptimise_Sequence();
            }
        } else {
            setup_8F_capture_basic_Sequence();
        }
        Analytics.sendAnalyticsEvents(Analytics.verbosity.DEBUG, Analytics.Cat.EXPORT_CONF, ExportConfig.getConfig());
        Analytics.sendAnalyticsEvents(Analytics.verbosity.DEBUG, Analytics.Cat.SEQUENCE, "8F Capture Sequence");
    }

    /*
        Create a capture sequence for extracting export templates from both hands
        One image for right and left. Liveness can be on / off
    */
    private void setup_8F_capture_basic_Sequence() {
        boolean rightHand = false;
        LivenessType livenessType = null;
        ExportConfig.setLaxLiveness(false);
        ExportConfig.setUseLiveness(false);
        if (ExportConfig.getUseLiveness()) {
            livenessType = LivenessType.STEREO_HORZ;
        } else {
            livenessType = LivenessType.NONE;
        }

        OptimiseMode optimiseMode = getOptimiseModeFromConfig();
        CaptureConfig.JoinMergeFormat = ExportConfig.getFormat();

        CaptureConfig firstImage = new CaptureConfig(FourFInterface.CaptureType.EIGHTF_LEFT,
                ExportConfig.getFormat(), rightHand, livenessType);
        firstImage.setOptimiseMode(optimiseMode);
        firstImage.setShowProcessingScreen(true);
        firstImage.setShowSwitchHands(true);
        firstImage.setAdd_to_8F_fuse_queue(true);

        rightHand = true;
        CaptureConfig secondImage = new CaptureConfig(FourFInterface.CaptureType.EIGHTF_RIGHT,
                ExportConfig.getFormat(), rightHand, livenessType);
        secondImage.setOptimiseMode(optimiseMode);
        secondImage.setAdd_to_8F_fuse_queue(true);
        secondImage.setShowProcessingScreen(true);

        captureSequence.clear();
        captureSequence.add(firstImage);
        captureSequence.add(secondImage);

        outputSlots = Arrays.asList(2);
        Analytics.sendAnalyticsEvents(Analytics.verbosity.DEBUG, Analytics.Cat.EXPORT_CONF, ExportConfig.getConfig());
        Analytics.sendAnalyticsEvents(Analytics.verbosity.DEBUG, Analytics.Cat.SEQUENCE, "8F Capture Basic Sequence");
    }

    private void setup_2F_capture_basic_Sequence() {
        boolean rightHand = false;
        LivenessType livenessType = LivenessType.NONE;

        CaptureConfig.JoinMergeFormat = ExportConfig.getFormat();

        ExportConfig.setReliabilityMask(ExportConfig.getReliabilityMask());
        ExportConfig.setPackExtraScale(ExportConfig.getPackExtraScale());

        CaptureConfig firstImage = new CaptureConfig(FourFInterface.CaptureType.NONE, ExportConfig.getFormat(), rightHand, livenessType);
        firstImage.setOptimiseMode(OptimiseMode.THUMB);
        firstImage.setShowProcessingScreen(true);
        firstImage.setShowSwitchHands(true);
        firstImage.setAdd_to_8F_fuse_queue(true);

        rightHand = true;
        CaptureConfig secondImage = new CaptureConfig(FourFInterface.CaptureType.NONE, ExportConfig.getFormat(), rightHand, livenessType);
        secondImage.setOptimiseMode(OptimiseMode.THUMB);
        secondImage.setShowProcessingScreen(true);
        secondImage.setAdd_to_8F_fuse_queue(true);

        captureSequence.clear();
        captureSequence.add(firstImage);
        captureSequence.add(secondImage);

        outputSlots = Arrays.asList(2);
        Analytics.sendAnalyticsEvents(Analytics.verbosity.DEBUG, Analytics.Cat.EXPORT_CONF, ExportConfig.getConfig());
        Analytics.sendAnalyticsEvents(Analytics.verbosity.DEBUG, Analytics.Cat.SEQUENCE, "2F Capture Basic Sequence");
    }

    private void setup_1F_capture_basic_Sequence() {
        boolean rightHand = ExportConfig.getCaptureHandSide() == ExportConfig.CaptureHand.RIGHT;
        LivenessType livenessType = LivenessType.NONE;
        CaptureConfig firstImage = new CaptureConfig(FourFInterface.CaptureType.NONE, ExportConfig.getFormat(), rightHand, livenessType);
        firstImage.setOptimiseMode(OptimiseMode.THUMB);
        firstImage.setShowProcessingScreen(true);
        firstImage.setAllowUserHandSwitch(!ExportConfig.getCaptureHandFixed());

        captureSequence.clear();
        captureSequence.add(firstImage);

        outputSlots = Arrays.asList(0);
        Analytics.sendAnalyticsEvents(Analytics.verbosity.DEBUG, Analytics.Cat.EXPORT_CONF, ExportConfig.getConfig());
        Analytics.sendAnalyticsEvents(Analytics.verbosity.DEBUG, Analytics.Cat.SEQUENCE, "1F Capture Basic Sequence");
    }


    /*
        8F capture, but with vertical liveness and finger fusing.
        No Liveness here. 4 separate shots.
     */
    private void setup_8F_capture_DoubleOptimise_Sequence() {
        Log.d(LOG_TAG, "Double optimise sequence");
        CaptureConfig.JoinMergeFormat = ExportConfig.getFormat();

        //-----------------------
        boolean rightHand = false;
        LivenessType livenessType = LivenessType.NONE;
        OptimiseMode optimiseMode = OptimiseMode.INDEX_MIDDLE;
        CaptureConfig firstImage = new CaptureConfig(FourFInterface.CaptureType.NONE,
                ExportConfig.getFormat(), rightHand, livenessType);
        firstImage.setOptimiseMode(optimiseMode);
        firstImage.setAdd_to_multishot_fuse_queue(true);
        firstImage.setShowProcessingScreen(true);
        //----------------------

        rightHand = false;
        livenessType = LivenessType.NONE;
        optimiseMode = OptimiseMode.RING_LITTLE;
        CaptureConfig secondImage = new CaptureConfig(FourFInterface.CaptureType.NONE,
                ExportConfig.getFormat(), rightHand, livenessType);
        secondImage.setOptimiseMode(optimiseMode);
        secondImage.setShowSwitchHands(true);
        secondImage.setAdd_to_multishot_fuse_queue(true);
        secondImage.setMultishot_to_8F_queue(true);
        secondImage.setShowProcessingScreen(true);

        //---------------------

        rightHand = true;
        livenessType = LivenessType.NONE;
        optimiseMode = OptimiseMode.INDEX_MIDDLE;
        CaptureConfig thirdImage = new CaptureConfig(FourFInterface.CaptureType.NONE,
                ExportConfig.getFormat(), rightHand, livenessType);
        thirdImage.setOptimiseMode(optimiseMode);
        thirdImage.setAdd_to_multishot_fuse_queue(true);
        thirdImage.setShowProcessingScreen(true);

        //---------------------

        rightHand = true;
        livenessType = LivenessType.NONE;
        optimiseMode = OptimiseMode.RING_LITTLE;
        CaptureConfig forthImage = new CaptureConfig(FourFInterface.CaptureType.NONE,
                ExportConfig.getFormat(), rightHand, livenessType);
        forthImage.setOptimiseMode(optimiseMode);
        forthImage.setAdd_to_multishot_fuse_queue(true);
        forthImage.setMultishot_to_8F_queue(true);
        forthImage.setShowProcessingScreen(true);

        //--------------------

        captureSequence.clear();
        captureSequence.add(firstImage);
        captureSequence.add(secondImage);
        captureSequence.add(thirdImage);
        captureSequence.add(forthImage);

        outputSlots = Arrays.asList(6);

        Analytics.sendAnalyticsEvents(Analytics.verbosity.DEBUG, Analytics.Cat.EXPORT_CONF, ExportConfig.getConfig());
        Analytics.sendAnalyticsEvents(Analytics.verbosity.DEBUG, Analytics.Cat.SEQUENCE, "8F Capture Double Optimise Sequence");
    }

    /*
        8F capture, but with vertical liveness and finger fusing.
        Uses Vertical Liveness to allow just two shots
     */
    private void setup_8F_capture_DoubleOptimise_Liveness_Sequence() {
        Log.d(LOG_TAG, "Double optimise sequence with liveness");
        CaptureConfig.JoinMergeFormat = ExportConfig.getFormat();

        //-----------------------
        boolean rightHand = false;
        LivenessType livenessType = LivenessType.STEREO_VERT;
        CaptureConfig firstImage = new CaptureConfig(FourFInterface.CaptureType.NONE,
                ExportConfig.getFormat(), rightHand, livenessType);

        firstImage.setAdd_to_multishot_fuse_queue(true);

        firstImage.setGetTemplateFromSecondLivenessImage(true);
        firstImage.setAdditionalTemplateformat(ExportConfig.getFormat());
        firstImage.setSecondLiveness_to_multishot_queue(true);
        firstImage.setMultishot_to_8F_queue(true);

        firstImage.setShowProcessingScreen(true);
        firstImage.setShowSwitchHands(true);

        //---------------------

        rightHand = true;
        livenessType = LivenessType.STEREO_VERT;
        CaptureConfig secondImage = new CaptureConfig(FourFInterface.CaptureType.NONE,
                ExportConfig.getFormat(), rightHand, livenessType);

        secondImage.setAdd_to_multishot_fuse_queue(true);

        secondImage.setGetTemplateFromSecondLivenessImage(true);
        secondImage.setAdditionalTemplateformat(ExportConfig.getFormat());
        secondImage.setSecondLiveness_to_multishot_queue(true);
        secondImage.setMultishot_to_8F_queue(true);

        secondImage.setShowProcessingScreen(true);

        captureSequence.clear();
        captureSequence.add(firstImage);
        captureSequence.add(secondImage);

        outputSlots = Arrays.asList(4); // should be 4

        Analytics.sendAnalyticsEvents(Analytics.verbosity.DEBUG, Analytics.Cat.EXPORT_CONF, ExportConfig.getConfig());
        Analytics.sendAnalyticsEvents(Analytics.verbosity.DEBUG, Analytics.Cat.SEQUENCE, "8F Capture Double Optimise Liveness Sequence");
    }

    /*
    Create a capture sequence for extracting export templates from one hand
    */
    private void setup_captureSequence() {
        boolean rightHand = ExportConfig.getCaptureHandSide() == ExportConfig.CaptureHand.RIGHT;
        ExportConfig.setLaxLiveness(false);
        ExportConfig.setUseLiveness(false);
        FourFInterface.CaptureType type = rightHand ? FourFInterface.CaptureType.EIGHTF_RIGHT : FourFInterface.CaptureType.EIGHTF_LEFT;
        LivenessType livenessType = null;
        if (ExportConfig.getUseLiveness()) {
            livenessType = LivenessType.STEREO_HORZ;
        } else {
            livenessType = LivenessType.NONE;
        }
        OptimiseMode optimiseMode = getOptimiseModeFromConfig();

        CaptureConfig firstImage = new CaptureConfig(type, ExportConfig.getFormat(), rightHand, livenessType);
        firstImage.setOptimiseMode(optimiseMode);
        firstImage.setAllowUserHandSwitch(!ExportConfig.getCaptureHandFixed());
        firstImage.setShowProcessingScreen(true);
        captureSequence.clear();

        outputSlots = Arrays.asList(0);
        captureSequence.add(firstImage);

        Analytics.sendAnalyticsEvents(Analytics.verbosity.DEBUG, Analytics.Cat.EXPORT_CONF, ExportConfig.getConfig());
        Analytics.sendAnalyticsEvents(Analytics.verbosity.DEBUG, Analytics.Cat.SEQUENCE, "Capture Sequence");
    }

    private OptimiseMode getOptimiseModeFromConfig() {
        if (ExportConfig.getOptimiseForIndex()) {
            return OptimiseMode.INDEX_MIDDLE;
        } else {
            return OptimiseMode.NONE;
        }
    }

    private static InMemoryKVStore sMemoryKVStore = new InMemoryKVStore();

    protected IKVStore openStorage() {
        return sMemoryKVStore;
    }

    // Called at each kickoff event
    @Override
    protected void configureBiometricEngine(IBiometricsEngine<ImageHolder, RectF[]> engine) {
        if (currentConfig.getLivenessType() == LivenessType.STEREO_VERT) {
            currentConfig.setOptimiseMode(OptimiseMode.RING_LITTLE);
        }
        if (currentConfig.getOptimiseMode() == OptimiseMode.THUMB) {
            thumbProcessor = new ThumbProcessor(this, FOURF_TIMEOUT, getTemplatesCount(), async, this, this);
            thumbProcessor.setConfig(currentConfig);

            ChainedHandler<ImageHolder, byte[]> handler = new ChainedHandler<>();
            configureFourFResultsHandler(handler);
            thumbProcessor.setResultHandler(handler);
            engine.addProcessor(thumbProcessor);
        } else {
            fourFProcessor = new FourFProcessor(this, FOURF_TIMEOUT, getTemplatesCount(), async, this, this);
            fourFProcessor.setConfig(currentConfig);

            ChainedHandler<ImageHolder, byte[]> handler = new ChainedHandler<>();
            configureFourFResultsHandler(handler);
            fourFProcessor.setResultHandler(handler);
            engine.addProcessor(fourFProcessor);
        }
    }

    protected TemplateStorageInMemory temporaryStorage = null; // temp slot for sending templates to handlers
    protected TemplateStorageInMemory additionalTemplateStorage = null; // temp slot for sending templates to handlers
    protected TemplateStorageInMemory temporaryStorage_liveness2 = null; // temp slot for sending templates to handlers, from the second liveness image
    protected TemplateStorageInMemory additionalTemplateStorage_liveness2 = null; // temp slot for sending templates to handlers, from second liveness image

    // Set up the chained handlers according to the currentConfig
    // If the chain breaks at any point the capture sequence will fail
    protected void configureFourFResultsHandler(ChainedHandler<ImageHolder, byte[]> chainedHandler) {

        Log.d(LOG_TAG, "Config results handler");

        // Done inside fourf processor so we can send a liveness fail event
        //if(currentConfig.isUseLiveness()){
        //chainedHandler.addSuccessor(new LocalFourFLivenessCheckerRH<ImageHolder>(currentConfig.getFormat())); // check liveness
        //}

        if (currentConfig.isMatchWithStored()) {
            LocalBiometricMatcher matcher = createMatcher();
            TemplateProviderFromTemplateStorage templateProvider = new TemplateProviderFromTemplateStorage(temporaryStorage);
            matcher.setTemplatedProvider(templateProvider);
            chainedHandler.addSuccessor(new AuthenticationHandler<ImageHolder>(matcher)); // match first acquired template against the one from the review step
        }

        if (currentConfig.isStoreForMatch()) {
            temporaryStorage = new TemplateStorageInMemory();
            chainedHandler.addSuccessor(new PersistEnrollmentHandler<ImageHolder>(temporaryStorage));
        }

        if (currentConfig.isGetTemplateFromLivenessImage()) {
            additionalTemplateStorage = new TemplateStorageInMemory();
            chainedHandler.addSuccessor(new LocalFourFGetLivenessTemplateRH<ImageHolder>(additionalTemplateStorage, currentConfig.getAdditionalTemplateformat().getCode(), ExportConfig.getConfig()));
        } else if (currentConfig.isGetTemplateFromStoredImage()) {
            additionalTemplateStorage = new TemplateStorageInMemory();
            chainedHandler.addSuccessor(new LocalFourFGetTemplateInternalRH<ImageHolder>(additionalTemplateStorage, currentConfig.getAdditionalTemplateformat().getCode(), ExportConfig.getConfig()));
        }

        if (currentConfig.isGetTemplateFromSecondLivenessImage()) { // get an additional template from the second liveness image
            additionalTemplateStorage_liveness2 = new TemplateStorageInMemory();
            chainedHandler.addSuccessor(new LocalFourFGetSecondLivenessTemplateRH<ImageHolder>(additionalTemplateStorage_liveness2, currentConfig.getAdditionalTemplateformat().getCode(), ExportConfig.getConfig()));
        }

        if (currentConfig.isSecondLivenessImageAsEnrol() && currentConfig.getLivenessType() == LivenessType.STEREO_VERT) { // get a VFP template from the second liveness image
            // get a VFP template from second liveness image
            temporaryStorage_liveness2 = new TemplateStorageInMemory();
            chainedHandler.addSuccessor(new LocalFourFGetSecondLivenessTemplateRH<ImageHolder>(temporaryStorage_liveness2, TemplateFormat.FORMAT_VERIDFP.getCode(), ExportConfig.getConfig()));

            // match the second liveness image template to the one stored in persistence
            LocalBiometricMatcher matcher = createMatcher();
            TemplateProviderFromTemplateStorage templateProvider = new TemplateProviderFromTemplateStorage(temporaryStorage_liveness2);
            matcher.setTemplatedProvider(templateProvider);
            chainedHandler.addSuccessor(new AuthenticationHandler<ImageHolder>(matcher));

        }

        if (currentConfig.isMatchAgainstEnrol() || currentConfig.isStoreAsEnrolTemplate()) {
            //ITemplatesStorage storage = templates_store;
            TemplateProviderCache cacheProvider = new TemplateProviderCache(new TemplateProviderFromTemplateStorage(templates_store));

            if (currentConfig.isMatchAgainstEnrol()) {
                LocalBiometricMatcher matcher = createMatcher();
                matcher.setTemplatedProvider(cacheProvider);
                chainedHandler.addSuccessor(new AuthenticationHandler<ImageHolder>(matcher));
            }

            if (currentConfig.isStoreAsEnrolTemplate()) {
                chainedHandler.addSuccessor(new AdaptiveEnrollmentHandler<ImageHolder>(cacheProvider, templates_store, ADAPTIVE_TEMPLATES_LIMIT));
            }
        }

        // use the second liveness image as an enrol verification, and store as an enrol template
        if (currentConfig.isSecondLivenessImageAsEnrol()) {
            // save as an enrol template
            TemplateProviderCache cacheProvider = new TemplateProviderCache(new TemplateProviderFromTemplateStorage(templates_store));
            chainedHandler.addSuccessor(new AdaptiveEnrollmentHandlerLive<ImageHolder>(cacheProvider, templates_store, temporaryStorage_liveness2, ADAPTIVE_TEMPLATES_LIMIT));
        }

    }

    @Override
    protected void retry() {
        temporaryStorage = null;
        setUpInitialResults();
        super.retry();
    }

    private LocalBiometricMatcher createMatcher() {
        return new LocalFourFBiometricMatcher();
    }

    protected void openTemplateStorage() {
        templates_store_right = new BytesTemplatesStorage(FourFInterface.SUFFIX_KEY_ENROL_RIGHT, persistence);
        templates_store_left = new BytesTemplatesStorage(FourFInterface.SUFFIX_KEY_ENROL_LEFT, persistence);
        templates_store = new BytesTemplatesStorage("VOID", persistence); // past to handlers
    }

    @Override
    protected CameraSamplingPolicy getCameraPolicy() {
        return new DefaultFourFCameraPolicy();
    }

    @Override
    protected int getCurrentOptimiseMode() {
        return currentConfig.getOptimiseMode().getCode();
    }

    @Override
    public void onComplete(Map<String, BiometricsResult<ImageHolder>> results) {
        dismissDialog();
        DecentralizedBiometricsEngineImpl.startDetailedSampling = false;
        // stop the timers
        if (mCountDownTimer != null) {
            mCountDownTimer.stop();
        }
        if (mSwitchHandTimer != null) {
            mSwitchHandTimer.stop();
        }

        // add the results to previous results
        BiometricsResult new_result = results.get(FourFInterface.UID);
        previousResults.addResult((ImageHolder) new_result.getInputs()[0], new_result.getOutput(-1));

        // add second liveness image to multishot queue. Do first, as this is the index-middle we want
        if (currentConfig.isSecondLiveness_to_multishot_queue()) {
            try {
                byte[] added_template = additionalTemplateStorage_liveness2.restore()[0];
                Log.d(LOG_TAG, "added second liveness template to multishot queue");
                multishot_fuse_queue.addResult((ImageHolder) new_result.getInputs()[0], added_template);
            } catch (IOException e) {
                e.printStackTrace();
                onError(0, "failed to retrieve additionalTemplateStorage");
            }
        }

        // there is another template to save in from the additionalTemplateStorage
        if (currentConfig.isGetTemplateFromStoredImage() || currentConfig.isGetTemplateFromLivenessImage()) {
            Log.d(LOG_TAG, "Adding template generated from stored");
            try {
                byte[] added_template = additionalTemplateStorage.restore()[0];
                previousResults.addResult((ImageHolder) new_result.getInputs()[0], added_template);
                if (currentConfig.isStoreTemplate_to_multishot_queue()) {
                    Log.d(LOG_TAG, "added additional store template to multishot queue");
                    multishot_fuse_queue.addResult((ImageHolder) new_result.getInputs()[0], added_template);
                }
            } catch (IOException e) {
                e.printStackTrace();
                onError(0, "failed to retrieve additionalTemplateStorage");
            }
        }

        // merge last two templates if specified. Gets added to previous results
        if (currentConfig.isAdd_to_multishot_fuse_queue()) {
            Log.d(LOG_TAG, "Add to multi-shot queue");
            multishot_fuse_queue.addResult((ImageHolder) new_result.getInputs()[0], new_result.getOutput(-1));
        }


        // merge last two templates if specified. Gets added to previous results
        if (currentConfig.isAdd_to_8F_fuse_queue()) {
            Log.d(LOG_TAG, "Add to 8F fuse queue");
            eightF_fuse_queue.addResult((ImageHolder) new_result.getInputs()[0], new_result.getOutput(-1));
        }

        // run fusing if queued
        if (multishot_fuse_queue.size() >= 2) mergeTemplates(multishot_fuse_queue);
        if (eightF_fuse_queue.size() >= 2) joinTemplates(eightF_fuse_queue);

        // see if we have completed the capture list, put all previous results into finalResults
        // and clear out the 4F lib
        boolean complete = false;
        if (currentConfigIndex == captureSequence.size() - 1) {
            onCaptureSequenceComplete();
            complete = true;
        }

        // move to next capture config
        currentConfigIndex++;

        // Show an intermediate fragment. Need to move this into the capture config.
        if ((currentConfig.getCaptureType() == FourFInterface.CaptureType.ENROLMENT_ONE)) {
            onEnrollmentStep1Complete();
        } else if (currentConfig.getCaptureType() == FourFInterface.CaptureType.ENROLMENT_TWO) {
            onEnrollmentStep2Complete();
        } else if (currentConfig.getCaptureType() == FourFInterface.CaptureType.AUTH) {
            showDialogWithMode(AUTHENTICATION_COMPLETE);
        } else if (complete) {
            showDialogWithMode(CAPTURE_COMPLETE);
        } else if (currentConfig.isShowSwitchHands()) {
            showDialogWithMode(SWITCH_HANDS);
        } else if (ExportConfig.getOptimiseForIndexLittle() || isEnrollExport()) {
            if (!currentConfig.isRightHand()) {
                if (currentConfig.getOptimiseMode() == OptimiseMode.INDEX_MIDDLE) {
                    showDialogWithMode(INDEX_COMPLETE);
                } else {
                    showDialogWithMode(SWITCH_HANDS);
                }
            } else {
                if (currentConfig.getOptimiseMode() == OptimiseMode.INDEX_MIDDLE) {
                    showDialogWithMode(INDEX_COMPLETE);
                } else {
                    onEnrollmentStep2Complete();
                }
            }
        } else if (isCapture8F()) {
            if (!currentConfig.isRightHand()) {
                showDialogWithMode(SWITCH_HANDS);
            } else {
                showDialogWithMode(ENROLLMENT_COMPLETE);
            }
        } else {
            if (!complete) {
                kickOffBiometricsProcess();
            } else {
                super.onComplete(finalResults);
                return;
            }
        }
    }

    private void printBiometricsResult(BiometricsResult<ImageHolder> results) {
        int N = results.size();
        Log.d(LOG_TAG, "printBiometricsResult size:" + N);
        for (int i = 0; i < N; i++) {
            Log.d(LOG_TAG, "output " + i + ", length: " + results.getOutput(i).length);
        }
    }

    private void onCaptureSequenceComplete() {
        // When the capture list has been completed fill finalResults
        Log.d(LOG_TAG, "onCaptureSequenceComplete");
        printBiometricsResult(previousResults);

        ImageHolder[] inputs = previousResults.getInputs();
        byte[][] outputs = previousResults.getOutputs();

        for (int i : outputSlots) {
            if (i < 0 || i >= previousResults.size()) {
                onError(0, "outputSlots exceeds captured results");
            }
            outputResults.addResult((ImageHolder) inputs[0], outputs[i]);
        }

        finalResults = new HashMap<>();
        finalResults.put(FourFInterface.UID, outputResults);
        previousResults = null;
        FourFIntegrationWrapper.purge();// this could be a shutdown call
    }

    private int getTemplatesCount() {
        int templates;

        if (currentConfig.getLivenessType() == LivenessType.STEREO_VERT
                || currentConfig.getLivenessType() == LivenessType.STEREO_HORZ) {
            templates = 2; // liveness takes stereo images
        } else {
            templates = 1; // all other modes get one at a time
        }

        return templates;
    }

    protected void onPause() {
        super.onPause();
        pauseFourFProcessor();
    }

    private void pauseFourFProcessor() {
        if (fourFProcessor != null) {
            fourFFragment.tv_placeYourFingers.setVisibility(View.GONE);
            fourFProcessor.onPause();
            if (mBiometricsEngine != null) {
                mBiometricsEngine.pause();
            }
        }
    }

    protected void onResume() {
        super.onResume();
        resumeFourFProcessor();
    }

    @Deprecated
    public TemplateFormat getTemplateFormat() {
        return ExportConfig.getFormat();
    }

    @Deprecated
    public void setTemplateFormat(TemplateFormat newFormat) {
        ExportConfig.setFormat(newFormat);
    }

    protected DefaultFourFFragment fourFFragment;

    protected DefaultThumbFragment thumbFragment;

    /**
     * Access protected member roiRenderer
     *
     * @return Roi rendering object
     */
    protected RealtimeRoisDecorator getRoiRenderer() {
        return fourFFragment.roiRenderer;
    }

    protected IKVStore persistence;

    private static final String SUFFIX_AUTOSTART = "_autostart";
    private static final String SUFFIX_TIPS = "_tips";

    public void setAutoStart(boolean enable) {
        if (persistence != null) {
            persistence.update(meta.getUID() + SUFFIX_AUTOSTART, String.valueOf(enable).getBytes());
            persistence.commit(false);
        }
    }

    public void disableTips(boolean enabled) {
        if (persistence != null) {
            persistence.update(meta.getUID() + SUFFIX_TIPS, String.valueOf(enabled).getBytes());
            persistence.commit(false);
        }
    }

    public boolean tipsDisabled() {
        if (persistence != null) {
            String key = meta.getUID() + SUFFIX_TIPS;
            if (persistence.contains(key)) {
                byte[] data = persistence.read(key);
                if (data != null && data.length > 0) {
                    return Boolean.parseBoolean(new String(data));
                }
            }
        }
        return false;
    }

    public boolean isAutoStartEnabled() {
        if (persistence != null) {
            String key = meta.getUID() + SUFFIX_AUTOSTART;
            if (persistence.contains(key)) {
                byte[] data = persistence.read(key);
                if (data != null && data.length > 0) {
                    return Boolean.parseBoolean(new String(data));
                }
            }
        }
        return AUTO_START_DEFAULT;
    }

    private void clearEnrollement() {
        HandGuideHelper.clearGuides(this);
        try {
            templates_store_left.clear();
            templates_store_right.clear();
        } catch (Exception ex) {
            ex.printStackTrace();
            onError(0, ex.getMessage());
        }
    }

    // Do not override this method. Please use kickOffFourFFragment() to show a custom
    // fourf fragment with custom layout (extends DefaultFourFFragment)
    @Override
    public void kickOffBiometricsProcess() {
        if (captureSequence.size() < 1) {
            onError(0, "The capture sequence is empty");
            return;
        }

        currentConfig = captureSequence.get(currentConfigIndex); // set config for this capture

        //Below is horrible hack to force a delay in android 4 phones. This is because the decorators
        //are not shown if the delay is not added as the decorators are added in the wrong order.
        //This should be fixed with a more permanent solution.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            kickOffOptimisedFragment();
        } else {
            new CountDownTimer(400, 1) {
                public void onTick(long millisUntilFinished) {
                }

                public void onFinish() {
                    kickOffOptimisedFragment();
                }
            }.start();
        }

    }

    private void kickOffOptimisedFragment() {
        if (currentConfig.getOptimiseMode() == OptimiseMode.THUMB) {
            kickOffThumbFragment();
        } else {
            kickOffFourFFragment();
        }
    }

    /*
       Display the default 4F capture screen
       Override this method to show a custom 4F fragment
       (UICust)
     */
    public void kickOffFourFFragment() {
        showFragment(new DefaultFourFFragment());
    }

    /*
       Display the default Thumb capture screen
       Override this method to show a custom thumb fragment
       (UICust)
     */
    public void kickOffThumbFragment() {
        showFragment(new DefaultThumbFragment());
    }

    private void setupButtons() {
        fourFFragment.rl_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(LOG_TAG, "Cancelling");
                cancel();
            }
        });


        if (!isAutoStartEnabled()) {
            View.OnClickListener listener = new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startPreviewProcessing();
                }
            };
        } else {
        }

        fourFFragment.rl_switchHand.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!currentConfig.isRightHand()) { // change to right
                    CaptureConfig.setRightHandChoice(true); // static user choice. Used when allow user choice is true
                    setHand(true);
                } else if (currentConfig.isRightHand()) { // change to left hand
                    CaptureConfig.setRightHandChoice(false); // static user choice. Used when allow user choice is true
                    setHand(false);
                }

                currentConfig.setUserSelectedHand(true);
            }
        });
    }


    private void setHand(boolean useRightHand) {
        if (useRightHand) templates_store.updateKey(FourFInterface.SUFFIX_KEY_ENROL_RIGHT);
        else templates_store.updateKey(FourFInterface.SUFFIX_KEY_ENROL_LEFT);

        if (fourFProcessor != null) fourFProcessor.updateDefaultFocusRegion(useRightHand);
        if (currentConfig.getOptimiseMode() != OptimiseMode.THUMB) {
            updateSwitchHandUI();
            setGuideImageHand(useRightHand);
        } else {
            updateSwitchThumbUI();
            if (useRightHand) {
                thumbFragment.iv_handSide.setImageDrawable(getResources().getDrawable(R.drawable.righthand_thumb));
                thumbFragment.tv_handside.setText(getString(R.string.right_thumb));
            } else {
                thumbFragment.iv_handSide.setImageDrawable(getResources().getDrawable(R.drawable.lefthand_thumb_instructional));
                thumbFragment.tv_handside.setText(getString(R.string.left_thumb));
            }
        }
    }

    private void updateSwitchHandUI() {
        Log.d(LOG_TAG, "updateSwitchHandUI");
        //updateCountDownUI();
        if (fourFFragment != null) {
            if (currentConfig.isAllowUserHandSwitch()) {
                fourFFragment.rl_switchHand.setVisibility(View.VISIBLE);
            } else {
                fourFFragment.rl_switchHand.setVisibility(View.INVISIBLE);
                return;
            }
        }
    }

    private void updateSwitchThumbUI() {
        Log.d(LOG_TAG, "updateSwitchThumbUI");
        if (thumbFragment != null) {
            if (currentConfig.isAllowUserHandSwitch()) {
                thumbFragment.rl_switchHand.setVisibility(View.VISIBLE);
            } else {
                thumbFragment.rl_switchHand.setVisibility(View.INVISIBLE);
                return;
            }
        }
    }

    private void setGuideImageHand(boolean isRightHand) {
        float currentScale = fourFFragment.iv_imgFingerHint.getScaleX();
        Log.d(LOG_TAG, "poi finger hint scale: " + currentScale);
        if (isRightHand) {
            fourFFragment.iv_imgFingerHint.setScaleX(abs(currentScale) * -1.0f);
        } else {
            fourFFragment.iv_imgFingerHint.setScaleX(abs(currentScale));
        }
    }

    protected FrameLayout getCameraLayout() {
        if (currentConfig.getOptimiseMode() == OptimiseMode.THUMB) {
            return thumbFragment.mCameraLayout;
        } else {
            return fourFFragment.mCameraLayout;
        }
    }

    @Override
    protected List<CameraLayoutDecorator> createCameraLayoutDecorators() {
        if (currentConfig.getOptimiseMode() != OptimiseMode.THUMB) {
            List<CameraLayoutDecorator> list = new ArrayList<>();
            if (fourFFragment.roiRenderer == null) {
                synchronized (DefaultFourFBiometricsActivity.class) {
                    if (fourFFragment.roiRenderer == null) {
                        fourFFragment.roiRenderer = new RealtimeRoisDecorator(DefaultFourFBiometricsActivity.this, fourFFragment.mCameraLayout);
                    }
                }
            }

            list.add(fourFFragment.roiRenderer);

            list.add(new CameraLayoutDecorator() {
                @Override
                public void preAttachView() {

                }

                @Override
                public void postAttachView() {

                }

                @Override
                public void onCameraReady(float fov) {
                    DisplayMetrics dm = DisplayHelper.getDisplayMetrics(DefaultFourFBiometricsActivity.this);

                    int deviceWidth = dm.widthPixels;
                    int deviceHeight = dm.heightPixels;

                    float ratio = (float) deviceHeight / (float) deviceWidth;

                    setupHandGuide(fov, ratio);
                }

                @Override
                public void decorate(Canvas canvas, int w, int h) {

                }
            });

            return list;
        } else {
            List<CameraLayoutDecorator> list = new ArrayList<>();
            if (thumbFragment.roiRenderer == null) {
                synchronized (DefaultFourFBiometricsActivity.class) {
                    if (thumbFragment.roiRenderer == null) {
                        thumbFragment.roiRenderer = new RealtimeRoisDecorator(DefaultFourFBiometricsActivity.this, thumbFragment.mCameraLayout);
                    }
                }
            }

            list.add(thumbFragment.roiRenderer);


            list.add(new CameraLayoutDecorator() {
                @Override
                public void preAttachView() {

                }

                @Override
                public void postAttachView() {

                }

                @Override
                public void onCameraReady(float fov) {
                    DisplayMetrics dm = DisplayHelper.getDisplayMetrics(DefaultFourFBiometricsActivity.this);

                    int deviceWidth = dm.widthPixels;
                    int deviceHeight = dm.heightPixels;

                    float ratio = (float) deviceHeight / (float) deviceWidth;

                    setupThumbGuide(fov, ratio);
                }

                @Override
                public void decorate(Canvas canvas, int w, int h) {
                }
            });
            return list;
        }
    }

    protected void updateBiometricsStageMessage(final String displayMessage) {


        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (currentConfig.getOptimiseMode() != OptimiseMode.THUMB) {
                    fourFFragment.tv_placeYourFingers.setText(displayMessage);
                    if (fourFFragment.tv_placeYourFingers.getVisibility() != View.VISIBLE) {
                        fourFFragment.tv_placeYourFingers.setVisibility(View.VISIBLE);
                    }
                } else {
                    thumbFragment.tv_placeYourFingers.setText(displayMessage);
                    if (thumbFragment.tv_placeYourFingers.getVisibility() != View.VISIBLE) {
                        thumbFragment.tv_placeYourFingers.setVisibility(View.VISIBLE);
                    }
                }

            }
        });
    }

    protected void startPreviewProcessing() {
//        fourFFragment.tv_info.setVisibility(View.INVISIBLE);
//        fourFFragment.tv_info.setOnClickListener(null);
//        fourFFragment.rl_top.setOnClickListener(null);
        mCountDownTimer = new CustomCountDownTimer(getTimeout(), SECOND);
        mCountDownTimer.start();
        refreshTimer();
        if (dotsCountdown != null) {
            dotsCountdown.cancel();
        }

        updateprogressVisibility();


        super.startPreviewProcessing();
    }

    private int numberOfDots = 3;
    private CountDownTimer dotsCountdown;

    private void updateprogressVisibility() {

        dotsCountdown = new CountDownTimer(300000, 500) {

            public void onTick(long millisUntilFinished) {
                if (currentConfig.getOptimiseMode() != OptimiseMode.THUMB) {
                    String placeFingersString = (String) fourFFragment.tv_placeYourFingers.getText();
                    if (placeFingersString.equals(getString(R.string.place_fingers_in_template))) {
                        fourFFragment.tv_moveDots.setVisibility(View.VISIBLE);
                        if (numberOfDots == 1) {
                            fourFFragment.tv_moveDots.setText(".");
                            numberOfDots = 2;
                        } else if (numberOfDots == 2) {
                            fourFFragment.tv_moveDots.setText("..");
                            numberOfDots = 3;
                        } else if (numberOfDots == 3) {
                            fourFFragment.tv_moveDots.setText("...");
                            numberOfDots = 1;
                        }
                    } else {
                        fourFFragment.tv_moveDots.setVisibility(View.INVISIBLE);

                    }
                } else {
                    String placeFingersString = (String) thumbFragment.tv_placeYourFingers.getText();
                    if (placeFingersString.equals(getString(R.string.place_fingers_in_template))) {
                        thumbFragment.tv_moveDots.setVisibility(View.VISIBLE);
                        if (numberOfDots == 1) {
                            thumbFragment.tv_moveDots.setText(".");
                            numberOfDots = 2;
                        } else if (numberOfDots == 2) {
                            thumbFragment.tv_moveDots.setText("..");
                            numberOfDots = 3;
                        } else if (numberOfDots == 3) {
                            thumbFragment.tv_moveDots.setText("...");
                            numberOfDots = 1;
                        }
                    } else {
                        thumbFragment.tv_moveDots.setVisibility(View.INVISIBLE);

                    }
                }
            }

            public void onFinish() {
            }
        };
        dotsCountdown.start();
    }

    public void refreshTimer() {

        final Handler handler = new Handler();
        final Runnable counter = new Runnable() {

            public void run() {
                if (mCountDownTimer != null && mCountDownTimer.isStarted()) {
                    // update the timer when past
                    if (!timer_state && mCountDownTimer.getCurrentTime() < FOURF_TIMEOUT_WARN) {
                        timer_state = true;
                        //updateCountDownUI();
                    }
                    String timeout = Long.toString(mCountDownTimer.getCurrentTime() / SECOND);
//                    fourFFragment.tv_countDown.setText(timeout);
//                    fourFFragment.tv_countDownLeft.setText(timeout);
                    handler.postDelayed(this, SECOND);
                }
            }
        };

        handler.postDelayed(counter, SECOND);
    }

    protected void onEnrollmentStep1Complete() {
        showDialogWithMode(ENROLLMENT_STEP1_COMPLETE);
    }

    protected void onEnrollmentStep2Complete() {
        showDialogWithMode(ENROLLMENT_COMPLETE);
    }

    protected void onAuthenticationComplete() {
        showFragment(new DefaultFourFAuthenticationSuccessFragment());
    }

    protected void onCaptureComplete() {
        showFragment(new DefaultFourFCaptureCompleteFragment());
    }

    // Join the last two templates in the provided results (normally left and right hand. Works for any)
    private void joinTemplates(BiometricsResult<ImageHolder> results) {
        Log.d(LOG_TAG, "Join templates");

        int N = results.size();
        if (N < 2) {
            onError(0, "Not enough templates to join");
        }

        Log.d(LOG_TAG, "template 1 length: " + results.getOutput(N - 2).length);
        Log.d(LOG_TAG, "template 2 length: " + results.getOutput(N - 1).length);

        byte[] joinedTemplate = FourFIntegrationWrapper.JoinExportData(results.getOutput(N - 2),
                results.getOutput(N - 1), CaptureConfig.JoinMergeFormat.getCode());

        Log.d(LOG_TAG, "Joined template length: " + joinedTemplate.length);
        if (joinedTemplate.length == 1) { // need to check for an error
            int returnCode = joinedTemplate[0];
            if (returnCode != FourFInterface.JNIresult.SUCCESS.getCode()) {
                Log.e(LOG_TAG, "FourF ERROR Code = " + returnCode);
                onError(0, getString(R.string.eight_f_template_failed));
            }
        }

        previousResults.addResult((ImageHolder) results.getInputs()[0], joinedTemplate);
        results.clearResults();
    }

    // Merge the last two templates in the provided results
    private void mergeTemplates(BiometricsResult<ImageHolder> results) {
        Log.d(LOG_TAG, "Merge templates");

        int N = results.size();
        if (N < 2) {
            onError(0, "Not enough templates to merge");
        }

        Log.d(LOG_TAG, "template 1 length: " + results.getOutput(N - 2).length);
        Log.d(LOG_TAG, "template 2 length: " + results.getOutput(N - 1).length);

        byte[] mergedTemplate = FourFIntegrationWrapper.MergeExportData(results.getOutput(N - 2),
                results.getOutput(N - 1), CaptureConfig.JoinMergeFormat.getCode());

        Log.d(LOG_TAG, "Merged template length: " + mergedTemplate.length);
        if (mergedTemplate.length == 1) { // need to check for an error
            int returnCode = mergedTemplate[0];
            if (returnCode != FourFInterface.JNIresult.SUCCESS.getCode()) {
                Log.e(LOG_TAG, "FourF ERROR Code = " + returnCode);
                onError(0, getString(R.string.merge_f_template_failed));
            }
        }

        previousResults.addResult((ImageHolder) results.getInputs()[0], mergedTemplate);
        if (currentConfig.isMultishot_to_8F_queue()) {
            Log.d(LOG_TAG, "added multishot result to 8F queue");
            eightF_fuse_queue.addResult((ImageHolder) results.getInputs()[0], mergedTemplate);
        }
        results.clearResults();
    }

    @Override
    @UiThread
    public void clearRois() {
        fourFFragment.roiRenderer.clear();
        thumbFragment.roiRenderer.clear();
    }

    @Override
    @UiThread
    public void updateRois(final FourFInterface.TrackingState roiStatusCode, RectF[] newRois) {
        //boolean shouldClearROIS = false;
        String displayMessage;
        switch (roiStatusCode) {
            case PREVIEW_STAGE_NORMAL:
                displayMessage = getString(place_fingers_in_template);
                break;
//            case FourFProcessor.PREVIEW_STAGE_OOF:
//                displayMessage = "Out of focus";
//                break;
            case PREVIEW_STAGE_PICTURE_REQUESTED:
                displayMessage = getString(R.string.taking_picture);
                //shouldClearROIS = true;
                break;
            case PREVIEW_STAGE_TOO_CLOSE:
                displayMessage = getString(R.string.Move_hand_further_away);
                break;
            case PREVIEW_STAGE_TOO_FAR:
                displayMessage = getString(R.string.Move_hand_closer);
                break;
            case PREVIEW_STAGE_FINGERS_APART:
                displayMessage = getString(R.string.Hold_fingers_together);
                break;
            case PREVIEW_STAGE_TOO_HIGH:
                displayMessage = getString(R.string.Move_hand_down);
                break;
            case PREVIEW_STAGE_TOO_LOW:
                displayMessage = getString(R.string.Move_hand_up);
                break;
            case PREVIEW_STAGE_TOO_LEFT:
                displayMessage = getString(R.string.Move_hand_right);
                break;
            case PREVIEW_STAGE_TOO_RIGHT:
                displayMessage = getString(R.string.Move_hand_left);
                break;
            case PREVIEW_STAGE_FRAME_DIM:
                displayMessage = "";
                break;
            case PREVIEW_STAGE_NOT_CENTERED:
                displayMessage = getString(R.string.Place_hand_inside_mask);
                break;
            default:
                displayMessage = "Case " + roiStatusCode;
                break;
        }

        if (fourFFragment != null && fourFFragment.roiRenderer != null) {
            reviewGuidanceImages(roiStatusCode);
            fourFFragment.roiRenderer.update(newRois, fourfCodeToRoiColor(roiStatusCode));
        } else if (thumbFragment != null && thumbFragment.roiRenderer != null) {
            thumbFragment.roiRenderer.update(newRois, fourfCodeToRoiColor(roiStatusCode));
        }

        updateBiometricsStageMessage(displayMessage);

    }

    private static final int normalRoiColor = Color.argb(128, 0, 0, 255);
    private static final int errorRoiColor = Color.argb(75, 255, 0, 0);
    private static final int lockedRoiColor = Color.argb(128, 255, 255, 0);
    private static final int finalRoiColor = Color.argb(75, 0, 255, 0);

    private int fourfCodeToRoiColor(FourFInterface.TrackingState roiStatusCode) {
        switch (roiStatusCode) {
            case PREVIEW_STAGE_NORMAL:
                //return normalRoiColor;
                return errorRoiColor;
            case PREVIEW_STAGE_STABILIZED:
                return finalRoiColor;
            case PREVIEW_STAGE_TAKING_PICTURE:
            case PREVIEW_STAGE_PICTURE_REQUESTED:
                return finalRoiColor;
        }
        return errorRoiColor;
    }

    public boolean onTouchEvent(MotionEvent event) {
        return false;
    }

    @Override
    public void onBackPressed() {
        cancel();
    }

    @Override
    public void onFailure() {
        if (isEnrollment()) {
            dismissDialog();
            clearEnrollement();
            showDialogWithMode(FAILED_SCAN);
        } else {
            super.onFailure();
        }
    }

    @Override
    public void onError(int code, String message) {
        Log.d(LOG_TAG, "Fourf on error override");
        if (isEnrollment() || isEnrollExport()) {
            clearEnrollement();
        }
        super.onError(code, message);
    }

    public void onFourFFragmentReady(final DefaultFourFFragment fourFFragment) {
        this.fourFFragment = fourFFragment;
        setupButtons();
        setHand(currentConfig.isRightHand());
        super.kickOffBiometricsProcess();
    }

    public void onThumbFragmentReady(final DefaultThumbFragment thumbFragment) {
        this.thumbFragment = thumbFragment;
        setupThumbCancel();
        setHand(currentConfig.isRightHand());
        if (currentConfig.isRightHand()) {
            thumbFragment.iv_handSide.setImageDrawable(getResources().getDrawable(R.drawable.righthand_thumb));
            thumbFragment.tv_handside.setText(getString(R.string.right_thumb));
        }
        super.kickOffBiometricsProcess();
    }

    private void setupThumbCancel() {
        thumbFragment.rl_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(LOG_TAG, "Cancelling");
                cancel();
            }
        });

        thumbFragment.rl_switchHand.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!currentConfig.isRightHand()) {
                    CaptureConfig.setRightHandChoice(true);
                    setHand(true);

                } else if (currentConfig.isRightHand()) {
                    CaptureConfig.setRightHandChoice(false);
                    setHand(false);
                }
            }
        });
    }

    public void onReviewFragmentReady(DefaultFourFReviewInitialEnrollmentFragment reviewInitialEnrollmentFragment) {
        reviewInitialEnrollmentFragment.test.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                kickOffBiometricsProcess();
            }
        });
        reviewInitialEnrollmentFragment.cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cancel();
            }
        });
    }

    private long start = 0;

    public void onWaitingProcessingFragmentReady(DefaultFourFWaitForProcessingFragment waitForProcessingFragment) {
        Log.d(LOG_TAG, "waitingProcessingFragment displayed in " + (System.currentTimeMillis() - start) + "ms");
        //do nothing
    }

    public void onEnrollmentCompleteReady(DefaultFourFEnrollmentCompleteFragment enrollmentCompleteFragment) {
        enrollmentCompleteFragment.btn_continue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DefaultFourFBiometricsActivity.super.onComplete(finalResults);
            }
        });
    }

    public void onCaptureCompleteReady(DefaultFourFCaptureCompleteFragment captureCompleteFragment) {
        captureCompleteFragment.btn_continue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DefaultFourFBiometricsActivity.super.onComplete(finalResults);
            }
        });
    }

    public void onAuthSuccessFragmentReady(DefaultFourFAuthenticationSuccessFragment authSuccessFragment) {
        authSuccessFragment.bt_OK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DefaultFourFBiometricsActivity.super.onComplete(finalResults);
            }
        });
    }

    public void onFailedFragmentReady(DefaultFourFEnrollmentFailedFragment enrollmentFailedFragment) {
        mCountDownTimer.stop();
        if (currentConfig.getOptimiseMode() == OptimiseMode.THUMB) {
            enrollmentFailedFragment.advice.setText(getString(R.string.activity_thumb_failed_advice));
            enrollmentFailedFragment.failed.setText(getString(R.string.the_camera_failed_to_focus_thumb));
        }
        enrollmentFailedFragment.cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DefaultFourFBiometricsActivity.super.onFailure();
            }
        });
    }

    public void onLivenessInstructionalFragmentReady(final DefaultFourFLivenessFailedInstructionalFragment instructionalFragment) {

        instructionalFragment.btn_getStarted.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                retry();
            }
        });
        instructionalFragment.btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    public void onInstructionalFragmentReady(DefaultFourFCaptureInstructionalFragment instructionalFragment) {

        instructionalFragment.btn_getStarted.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                kickOffBiometricsProcess();
            }
        });
        instructionalFragment.btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cancel();
            }
        });
    }

    public void onInstructionalThumbFragmentReady(final DefaultFourFCaptureInstructionalThumbFragment instructionalThumbFragment) {

        instructionalThumbFragment.btn_getStarted.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                kickOffBiometricsProcess();
            }
        });
        instructionalThumbFragment.btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cancel();
            }
        });
    }

    public void onCaptureInstructionalFragmentReady(DefaultFourFCaptureInstructionalFragment instructionalFragment) {

        View.OnClickListener clickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                kickOffBiometricsProcess();

            }
        };

        instructionalFragment.btn_getStarted.setOnClickListener(clickListener);

        // instructionalFragment.cancel.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                cancel();
//            }
//        });
//
//        if(meta.isOptional()) {
//            instructionalFragment.cancel.setText(R.string.skip);
//        }
    }

    public void setupHandGuide(float cameraFOV, float screenRatio) {
        if (fourFFragment != null) {
            boolean handGuideWasSet = HandGuideHelper.setHandGuide(this, cameraFOV, screenRatio, CameraHelper.getCmSubjectDistanceFromCam(), fourFFragment.iv_imgFingerHint, currentConfig.getOptimiseMode());
            if (!handGuideWasSet) {
                Log.e(LOG_TAG, "Hand guide generation failed");
            }
        }
    }

    public void setupThumbGuide(float cameraFOV, float screenRatio) {
        if (thumbFragment != null) {
            boolean handGuideWasSet = HandGuideHelper.setHandGuide(this, cameraFOV, screenRatio, CameraHelper.getCmSubjectDistanceFromCamForThumb(), thumbFragment.iv_imgFingerHint, currentConfig.getOptimiseMode());
            if (!handGuideWasSet) {
                Log.e(LOG_TAG, "Hand guide generation failed");
            }
        }
    }

    /*
    public void setDefaultHandGuide() {
        if (fourFFragment != null) {
            HandGuideHelper.setDefaultHandGuide(this, fourFFragment.iv_imgFingerHint, ExportConfig.getOptimiseForIndex());
        }
    }
*/
    public void setupStereoHandGuide() {
        if (fourFFragment != null) {
            HandGuideHelper.setStereoHandGuide(this, fourFFragment.iv_imgFingerHint, currentConfig.getOptimiseMode(), currentConfig.getLivenessType());
        }
    }

    protected void reviewGuidanceImages(FourFInterface.TrackingState guidanceImageIndex) {
        fourFFragment.setGuidanceSymbol(guidanceImageIndex);
    }

    /*
        Override to change the vibration behaviour. Eg. blank it to turn off.
        Or use some other photo success indication (user can remove their hand)
        (UICust)
     */
    public void doVibrate() {
        Vibrator v = (Vibrator) DefaultFourFBiometricsActivity.this.getSystemService(Context.VIBRATOR_SERVICE);
        v.vibrate(300);
    }

    public void onProcessingStart() {
        if (fourFFragment != null) {
            this.fourFFragment.tv_placeYourFingers.setVisibility(View.GONE);
        }
        Log.d(LOG_TAG, "onProcessingStart");
        showProcessingDialog();
    }

    /*
      Do not override. Calls onProcessingStart() when required, dismisses dialog boxes
     */
    @Override
    public void onProcessingStartInternal() {
        Log.d(LOG_TAG, "onProcessingStartInternal");
        doVibrate();  // Vibrate to indicate capture success.
        dismissDialog();
        if (currentConfig.isShowProcessingScreen()) {
            start = System.currentTimeMillis();
            onProcessingStart(); // show processing fragment.
        }
    }

    @Override
    public void onProcessingStop() {
        //this.fourFFragment.tv_placeYourFingers.setVisibility(View.VISIBLE);
        Log.d(LOG_TAG, "onProcessingStop");
        //dismissDialog(); //TODO move to the other fragments and dialogs. Don't dismiss until other dialog is ready
    }

    @Override
    public void onDigestionComplete() {
        // template extraction has finished
        Log.d(LOG_TAG, "onDigestionComplete");
    }

    @Override
    public void onStereo1Accepted() {
        Log.d(LOG_TAG, "onStereo1Accepted");
        doVibrate();
        if (currentConfig.isAllowUserHandSwitch()) {
            currentConfig.setAllowUserHandSwitch(false);
        }
        updateSwitchHandUI();
        setupStereoHandGuide();
    }

    @Override
    public void onLivenessFail() {
        Log.d(LOG_TAG, "Liveness failed");

        DefaultFourFLivenessFailedInstructionalFragment fragment = getDefaultFourFLivenessFailedInstructionalFragment();
        FourFInterface.LivenessType livenessType = currentConfig.getLivenessType();
        dismissDialog();
        Bundle args = new Bundle();
        args.putInt("livenessType", livenessType.getCode());
        fragment.setArguments(args);
        showFragment(fragment);
    }

    @Override
    public void onPassiveLivenessFail() {
        Log.d(LOG_TAG, "Passive Liveness failed");
        dismissDialog();
        pauseFourFProcessor();
        showDialogWithMode(PASSIVE_LIVENESS_FAILED);
    }


    public DefaultFourFLivenessFailedInstructionalFragment getDefaultFourFLivenessFailedInstructionalFragment() {
        return new DefaultFourFLivenessFailedInstructionalFragment();
    }

    @Override
    protected void onStop() {
        super.onStop();
        dismissDialog();
    }

    @Override
    public void onAttemptError() {
        showFragment(new DefaultFourFEnrollmentFailedFragment());
    }

    @Override
    protected void cancel() {
        if (isEnrollment() || isEnrollExport()) {
            // clear any enrolled templates
            clearEnrollement();
        }
        super.cancel();
    }

    /* Check the chosen format is supported
     * @return True if supported, else false
     */
    protected boolean checkFormatSupported() {
        Log.d(LOG_TAG, "checkFormatSupported : " + TemplateFormat.resolveFriendly(ExportConfig.getFormat()));
        if (isCapture()) {
            return checkFormatSupportedCapture4F(ExportConfig.getFormat());
        } else if (isCapture8F()) {
            if (ExportConfig.getOptimiseForIndexLittle()) {
                return checkFormatSupportedAllFingerOptimise(ExportConfig.getFormat());
            } else {
                return checkFormatSupportedCapture8F(ExportConfig.getFormat());
            }

        } else if (isCapture2THUMB() || isCaptureTHUMB()) {
            return checkFormatSupportedCaptureThumb(ExportConfig.getFormat());
        } else {
            return true;
        }
    }

    /* Check the chosen format is supported for 4F capture
     * @return True if supported, else false
     */
    public static boolean checkFormatSupportedCapture4F(TemplateFormat choosenFormat) {
        // supports all listed formats
        return true;
    }

    /* Check the chosen format is supported for 8F capture
     * @return True if supported, else false
     */
    public static boolean checkFormatSupportedCapture8F(TemplateFormat choosenFormat) {
        switch (choosenFormat) {
            case FORMAT_VERIDFP:
                return false;
            case FORMAT_NIST:
                return false;
            case FORMAT_INTERPOL:
                return false;
            case FORMAT_WSQRAW:
                return true;
            case FORMAT_ISO_4_2005:
                return true;
            case FORMAT_JSON:
                return true;
            default:
                return false;
        }
    }

    /* Check the chosen format is supported for thumb capture
 * @return True if supported, else false
 */
    public static boolean checkFormatSupportedCaptureThumb(TemplateFormat choosenFormat) {
        switch (choosenFormat) {
            case FORMAT_VERIDFP:
                return false;
            case FORMAT_NIST:
                return false;
            case FORMAT_INTERPOL:
                return false;
            case FORMAT_WSQRAW:
                return false;
            case FORMAT_ISO_4_2005:
                return false;
            case FORMAT_JSON:
                return true;
            default:
                return false;
        }
    }

    /* Check the chosen format is supported for 8F capture
 * @return True if supported, else false
 */
    public static boolean checkFormatSupportedAllFingerOptimise(TemplateFormat choosenFormat) {
        switch (choosenFormat) {
            case FORMAT_VERIDFP:
                return false;
            case FORMAT_NIST:
                return false;
            case FORMAT_INTERPOL:
                return false;
            case FORMAT_WSQRAW:
                return true;
            case FORMAT_ISO_4_2005:
                return false;
            case FORMAT_JSON:
                return true;
            default:
                return false;
        }
    }

    private void resumeFourFProcessor() {
        if (fourFProcessor != null) {
            fourFFragment.tv_placeYourFingers.setVisibility(View.VISIBLE);
            fourFProcessor.onResume();
            if (mBiometricsEngine != null) {
                mBiometricsEngine.resume();
            }
        }
    }

    private void dismissDialog() {
        if (dialog != null) {
            dialog.dismiss();
            dialog = null;
        }
    }

    private Dialog dialog;

    private void showProcessingDialog() {
        dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_processing);

        RelativeLayout background = (RelativeLayout) dialog.getWindow().getDecorView().findViewById(R.id.background_layout);
        Drawable draw = background.getBackground();
        draw = UICustomization.getImageWithDialogColor(draw);
        background.setBackground(draw);
        TextView processing = (TextView) dialog.getWindow().getDecorView().findViewById(R.id.tv_dialogProcessInfo);
        processing.setTextColor(UICustomization.getForegroundColor());
        ProgressBar progress = (ProgressBar) dialog.getWindow().getDecorView().findViewById(R.id.progressBar2);
        progress.getIndeterminateDrawable().setColorFilter(UICustomization.getBackgroundColor(), android.graphics.PorterDuff.Mode.SRC_IN);

        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogTheme;
        dialog.setCancelable(false);
        dialog.show();
    }

    private void showDialogWithMode(int mode) {
        if (fourFFragment != null) {
            fourFFragment.tv_placeYourFingers.setVisibility(View.GONE);
        }
        dialog = new Dialog(this);
        dialog.setCancelable(false);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_custom_fourf);

        RelativeLayout background = (RelativeLayout) dialog.getWindow().getDecorView().findViewById(R.id.mainDialog);
        Drawable draw = background.getBackground();
        draw = UICustomization.getImageWithDialogColor(draw);
        background.setBackground(draw);
        TextView mainText = (TextView) dialog.findViewById(R.id.tv_mainText);
        mainText.setTextColor(UICustomization.getForegroundColor());
        TextView smallText = (TextView) dialog.findViewById(R.id.tv_smallMessage);
        smallText.setTextColor(UICustomization.getForegroundColor());
        TextView cancel = (TextView) dialog.findViewById(R.id.tv_cancel);
        cancel.setTextColor(UICustomization.getForegroundColor());
        TextView next = (TextView) dialog.findViewById(R.id.tv_next);
        next.setTextColor(UICustomization.getForegroundColor());
        ImageView image = (ImageView) dialog.findViewById(R.id.imageView);
        ImageView image2 = (ImageView) dialog.findViewById(R.id.imageView2);

        cancel.setText(getString(R.string.cancel));

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                cancel();
            }
        });

        TextView tv_next = (TextView) dialog.findViewById(R.id.tv_next);
        next.setText(getString(R.string.next));
        View.OnClickListener nextClickListenerContinue = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                if (fourFFragment != null) {
                    fourFFragment.tv_placeYourFingers.setVisibility(View.VISIBLE);
                }
                kickOffBiometricsProcess();
            }
        };

        View.OnClickListener nextClickListenerFinish = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                if (fourFFragment != null) {
                    fourFFragment.tv_placeYourFingers.setVisibility(View.VISIBLE);
                }
                DefaultFourFBiometricsActivity.super.onComplete(finalResults);
            }
        };

        View.OnClickListener nextClickListenerRetry = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                if (fourFFragment != null) {
                    fourFFragment.tv_placeYourFingers.setVisibility(View.VISIBLE);
                }
                retry();
            }
        };

        if (mode == ENROLLMENT_STEP1_COMPLETE) {
            mainText.setText(getString(R.string.success_nfirst_scan_complete));
            smallText.setText(getString(R.string.please_verify_by_scanning_again));
            dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation_2;
            image.setImageDrawable(UICustomization.getImageWithForegroundColor(getResources().getDrawable(R.drawable.print_scan_success_no_tick)));
            image2.setImageDrawable(getResources().getDrawable(R.drawable.print_scan_success_only_tick));
            next.setOnClickListener(nextClickListenerContinue);
            dialog.show();
        } else if (mode == INDEX_COMPLETE) {
            mainText.setText(getString(R.string.success_n_scan_complete));
            smallText.setText(getString(R.string.please_move_hand_upwards_for_next_picture));
            dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation_2;
            image.setImageDrawable(UICustomization.getImageWithForegroundColor(getResources().getDrawable(R.drawable.print_scan_success_no_tick)));
            image2.setImageDrawable(getResources().getDrawable(R.drawable.print_scan_success_only_tick));
            next.setOnClickListener(nextClickListenerContinue);
            dialog.show();
        } else if (mode == SWITCH_HANDS) {
            mainText.setText(getString(R.string.success_nchange_hands));
            smallText.setText(getString(R.string.please_change_hands));
            dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation_2;
            image.setImageDrawable(UICustomization.getImageWithForegroundColor(getResources().getDrawable(R.drawable.circle_for_buttons)));
            image2.setImageDrawable(UICustomization.getImageWithForegroundColor(getResources().getDrawable(R.drawable.switch_hand_icon)));
            next.setOnClickListener(nextClickListenerContinue);
            dialog.show();
        } else if (mode == ENROLLMENT_COMPLETE) {
            mainText.setText(getString(R.string.success));
            smallText.setText(getString(R.string.you_have_completed_enrollment));
            dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation_2;
            image.setImageDrawable(UICustomization.getImageWithForegroundColor(getResources().getDrawable(R.drawable.fourf_enrollment_completed_no_tick)));
            image2.setImageDrawable(getResources().getDrawable(R.drawable.fourf_enrollment_completed_only_tick));
            next.setOnClickListener(nextClickListenerFinish);
            cancel.setVisibility(View.INVISIBLE);
            showCompleteDialog(dialog, mode);
        } else if (mode == AUTHENTICATION_COMPLETE) {
            mainText.setText(getString(R.string.success));
            smallText.setText(getString(R.string.you_have_completed_hand_nrecognition_authentication));
            dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation_2;
            image.setImageDrawable(UICustomization.getImageWithForegroundColor(getResources().getDrawable(R.drawable.fourf_enrollment_completed_no_tick)));
            image2.setImageDrawable(getResources().getDrawable(R.drawable.fourf_enrollment_completed_only_tick));
            next.setOnClickListener(nextClickListenerFinish);
            cancel.setVisibility(View.INVISIBLE);
            showCompleteDialog(dialog, mode);
        } else if (mode == FAILED_SCAN) {
            mainText.setText(getString(R.string.sorry));
            smallText.setText(getString(R.string.failed_to_verify));
            dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation_2;
            image.setImageDrawable(UICustomization.getImageWithForegroundColor(getResources().getDrawable(R.drawable.print_scan_failed_no_cross)));
            image2.setImageDrawable(getResources().getDrawable(R.drawable.print_scan_failed_only_cross));
            next.setText(getString(R.string.activity_fourf_failed_button_retry));
            next.setOnClickListener(nextClickListenerRetry);
            dialog.show();
        } else if (mode == CAPTURE_COMPLETE) {
            mainText.setText(getString(R.string.success));
            smallText.setText(getString(R.string.you_have_completed_hand_nrecognition_authentication));
            dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation_2;
            image.setImageDrawable(UICustomization.getImageWithForegroundColor(getResources().getDrawable(R.drawable.fourf_enrollment_completed_no_tick)));
            image2.setImageDrawable(getResources().getDrawable(R.drawable.fourf_enrollment_completed_only_tick));
            cancel.setVisibility(View.INVISIBLE);
            next.setOnClickListener(nextClickListenerFinish);
            showCompleteDialog(dialog, mode);
        } else if (mode == PASSIVE_LIVENESS_FAILED) {
            mainText.setText(getString(R.string.sorry));
            smallText.setText(getString(R.string.passive_failed));
            dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation_2;
            image.setImageDrawable(UICustomization.getImageWithForegroundColor(getResources().getDrawable(R.drawable.print_scan_failed_no_cross)));
            image2.setImageDrawable(getResources().getDrawable(R.drawable.print_scan_failed_only_cross));
            next.setText(getString(R.string.activity_fourf_failed_button_retry));
            next.setOnClickListener(nextClickListenerRetry);
            dialog.show();
        }
    }

    /*
        Override "complete dialog" behaviour (dialogs shown at the very end of capture sequences).
        Called for these actionId's:

        ENROLLMENT_COMPLETE
        CAPTURE_COMPLETE
        AUTHENTICATION_COMPLETE

        Display the dialog, or run auto key press on its buttons, eg. to skip showing the dialog.
        (next, cancel)
        See CustomFourFBiometricsActivity.java in demoappexport sample app
        (UICust)
     */
    protected void showCompleteDialog(Dialog mDialog, int actionId) {
        dialog.show();
    }
}

