package com.veridiumid.sdk.fourf.defaultui.activity;

import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;

/**
 * Created by josiah on 27/02/2017.
 */

public class UICustomization {

    private static int mBackgroundColor = 0xff1d91bc; // lightish blue
    private static int mForegroundColor = Color.WHITE;
    private static int mFingerGuideColor = 0xffBDBDBD;
    private static int mDialogColour = 0xff025783;

    private static Drawable mLogo = null;

    public static void setBackgroundColor(int backgroundColor) {
        mBackgroundColor = (backgroundColor);
    }

    public static void setForegroundColor(int foregroundColor) {
        mForegroundColor = (foregroundColor);
    }

    public static void setDialogColor(int dialogColor){
        mDialogColour = dialogColor;
    }

    //public static void setFingerColor(int fingerGuideColor) {
        //mFingerGuideColor = (fingerGuideColor);
    //}

    public static void setLogo(Drawable logo) {
        mLogo = logo;
    }

    protected static int getBackgroundColor() {
        return mBackgroundColor;
    }

    protected static int getDialogColor() {
        return mDialogColour;
    }

    protected static int getForegroundColor() {
        return mForegroundColor;
    }

    protected static Drawable getImageWithBackgroundColor(Drawable img) {
        img.setColorFilter(mBackgroundColor, PorterDuff.Mode.MULTIPLY);
        return img;
    }
    protected static Drawable getImageWithForegroundColor(Drawable img) {
        img.setColorFilter(mForegroundColor, PorterDuff.Mode.MULTIPLY);
        return img;
    }

    protected static Drawable getImageWithDialogColor(Drawable img) {
        img.setColorFilter(mDialogColour, PorterDuff.Mode.MULTIPLY);
        return img;
    }

    protected static void applyBackgroundColorMask(ImageView view) {
        view.setColorFilter(mBackgroundColor, PorterDuff.Mode.MULTIPLY);
    }

    protected static void applyForegroundColorMask(ImageView view) {
        view.setColorFilter(mForegroundColor, PorterDuff.Mode.MULTIPLY);
    }

    protected static void applyDialogColorMask(ImageView view) {
        view.setColorFilter(mDialogColour, PorterDuff.Mode.MULTIPLY);
    }

    protected static void applyFingerColorMask(ImageView view) {
        view.setColorFilter(mFingerGuideColor, PorterDuff.Mode.SRC_IN);
    }

    protected static Drawable getLogo() {
        return mLogo;
    }

}
