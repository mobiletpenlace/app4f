package com.veridiumid.sdk.fourf.defaultui.activity;

import android.graphics.PorterDuff;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.veridiumid.sdk.fourf.defaultui.R;
import com.veridiumid.sdk.support.base.VeridiumBaseFragment;

public class DefaultFourFWaitForProcessingFragment extends VeridiumBaseFragment {

    protected RelativeLayout root;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(com.veridiumid.sdk.fourf.defaultui.R.layout.layout_4f_wait_processing, container, false);
    }

    @Override
    protected void initView(View view) {
        super.initView(view);

        root = (RelativeLayout) view.findViewById(com.veridiumid.sdk.fourf.defaultui.R.id.layout_4f_review_root);
        ProgressBar processingProgress = (ProgressBar) view.findViewById(R.id.pb_processing_progress);

        // Change background of screen to background color, spinny to foreground color.
        root.setBackgroundColor(UICustomization.getBackgroundColor());

        if(processingProgress!=null){
            processingProgress.getIndeterminateDrawable().setColorFilter(UICustomization.getForegroundColor(), PorterDuff.Mode.SRC_IN);
        }

        ((DefaultFourFBiometricsActivity) baseActivity).onWaitingProcessingFragmentReady(this);
    }
}
