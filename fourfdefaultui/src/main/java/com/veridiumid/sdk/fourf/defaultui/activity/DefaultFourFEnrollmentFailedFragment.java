package com.veridiumid.sdk.fourf.defaultui.activity;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.veridiumid.sdk.fourf.defaultui.R;
import com.veridiumid.sdk.support.base.VeridiumBaseFragment;

public class DefaultFourFEnrollmentFailedFragment extends VeridiumBaseFragment {

    protected Button cancel;
    protected Button retry;
    protected TextView sorry;
    protected TextView tips;
    protected TextView advice;
    protected TextView failed;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.layout_4f_failed, container, false);
    }

    @Override
    protected void initView(View view) {
        super.initView(view);

        View root = (View) view.findViewById(R.id.activity_fourf_failed_base_layout);
        root.setBackgroundColor(UICustomization.getDialogColor());

        ImageView image = (ImageView)view.findViewById(R.id.imageView3);
        UICustomization.applyBackgroundColorMask(image);



        cancel = (Button) view.findViewById(R.id.activity_4f_button_cancel);
        cancel.setTextColor(UICustomization.getBackgroundColor());
        sorry = (TextView) view.findViewById(R.id.sorry);
        sorry.setTextColor(UICustomization.getForegroundColor());
        tips = (TextView) view.findViewById(R.id.tips);
        tips.setTextColor(UICustomization.getForegroundColor());
        advice = (TextView) view.findViewById(R.id.advice);
        advice.setTextColor(UICustomization.getForegroundColor());
        failed = (TextView) view.findViewById(R.id.failed_message);
        failed.setTextColor(UICustomization.getForegroundColor());


        ((DefaultFourFBiometricsActivity) baseActivity).onFailedFragmentReady(this);
    }
}
