package com.totalplay.fieldforcetp;


import android.Manifest;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.veridiumid.sdk.BiometricResultsParser;
import com.veridiumid.sdk.IBiometricFormats;
import com.veridiumid.sdk.IBiometricResultsHandler;
import com.veridiumid.sdk.IVeridiumSDK;
import com.veridiumid.sdk.VeridiumSDK;
import com.veridiumid.sdk.core.exception.LicenseException;
import com.veridiumid.sdk.fourf.FourFInterface;
import com.veridiumid.sdk.fourfintegration.ExportConfig;
import com.veridiumid.sdk.fourfintegration.FourFIntegrationWrapper;
import com.veridiumid.sdk.support.base.VeridiumBaseActivity;
import com.veridiumid.sdk.support.help.ToastHelper;

import java.io.File;
import java.io.FileOutputStream;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;

public class LoginActivity extends VeridiumBaseActivity {

    private static final String LOG_TAG = LoginActivity.class.getSimpleName();
    private static final int REQUEST_ENROLL = 3;
    private static final int REQUEST_ENROLL_EXPORT = 4;
    private static final int REQUEST_AUTH = 5;
    private static final int REQUEST_APP_SETTINGS = 168;
    private static final String[] requiredPermissions = new String[]{
            Manifest.permission.CAMERA,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_PHONE_STATE
            /* ETC.. */
    };
    private static int session_id;
    private static int first_session_id = 1;

    SharedPreferences sharedPref;
    private boolean isValid = false;

    private TextView tv_session_id;
    private String savePath = "/Veridium/TouchlessID_Export_Demo/";
    private File saveDir; // location to save biometric templates
    IBiometricResultsHandler customCaptureHandler = new IBiometricResultsHandler() {
        @Override
        public void handleSuccess(List<byte[]> results) {
            // Here we extract the template in the first element and send to be written to disk
            // The output template is always in the first slot
            if (results != null && results.size() > 0) {
                writeTemplate(results.get(0));
                ToastHelper.showMessage(LoginActivity.this, R.string.save_success);
                session_id++;
                updateSessionID();
            } else {
                ToastHelper.showMessage(LoginActivity.this, R.string.write_fail);
            }
        }

        @Override
        public void handleFailure() {
            ToastHelper.showMessage(LoginActivity.this, R.string.failed);
        }

        @Override
        public void handleLivenessFailure() {
            ToastHelper.showMessage(LoginActivity.this, R.string.liveness_failed);
        }

        @Override
        public void handleCancellation() {
            ToastHelper.showMessage(LoginActivity.this, R.string.cancelled);
        }

        @Override
        public void handleError(String message) {
            ToastHelper.showMessage(LoginActivity.this, getString(R.string.error) + message);
        }
    };
    private IVeridiumSDK mBiometricSDK;

    public static void launch(AppCompatActivity appCompatActivity) {
        Intent intent = new Intent(appCompatActivity, LoginActivity.class);
        appCompatActivity.startActivity(intent);
    }

    public static Intent createIntent(Context applicationContext) {
        return new Intent(applicationContext, LoginActivity.class);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // setToolbarEnabled(false);
        openAppPreferences();
        readAppData();
        setContentView(R.layout.activity_login);


        if (Build.VERSION.SDK_INT > 22 && !hasPermissions(requiredPermissions)) {
            ToastHelper.showMessage(LoginActivity.this, R.string.please_grant_all_permissions);
            goToSettings();
        } else {
            getSupportFragmentManager().beginTransaction().add(android.R.id.content, new SplashFragment(), null).commitAllowingStateLoss();
        }

        ExportConfig.setCalculate_NFIQ(false);
        ExportConfig.setBackground_remove(true);
        ExportConfig.setPackDebugInfo(false);
        ExportConfig.setPackExtraScale(true);
        ExportConfig.setPackAuditImage(true);
        ExportConfig.setLaxLiveness(false);  // set true to lower liveness requirements
        ExportConfig.setUseLiveness(false);  // set true to lower liveness requirements
        ExportConfig.setWSQCompressRatio(ExportConfig.WSQCompressRatio.COMPRESS_10to1);
        ExportConfig.setCaptureHand(ExportConfig.CaptureHand.LEFT);

        WebServices.services().loginUserTotalPlay("5a3049d113ccd", "finger_cfg_4", "").enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });


    }

    private void openAppPreferences() {
        sharedPref = this.getPreferences(this.MODE_PRIVATE);
    }

    private void writeAppData() {
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putInt(getString(R.string.saved_session_id), session_id);
        editor.commit();
        Log.d(LOG_TAG, "Wrote session id: " + session_id);
    }

    private void readAppData() {
        session_id = sharedPref.getInt(getString(R.string.saved_session_id), first_session_id);
        Log.d(LOG_TAG, "Read session id: " + session_id);
    }

    /* Set session id text to the last saved value and write current to storage
     */
    private void updateSessionID() {
        writeAppData();
        if (session_id > first_session_id) {
            tv_session_id.setText(String.format("%s%s", this.getString(R.string.fragment_home_session_id), String.format(" %03d", session_id - 1)));
        }
    }

    private void goToSettings() {
        Intent myAppSettings = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS, Uri.parse("package:" + getPackageName()));
        myAppSettings.addCategory(Intent.CATEGORY_DEFAULT);
        myAppSettings.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivityForResult(myAppSettings, REQUEST_APP_SETTINGS);
    }

    /* A custom IBiometricResultsHandler, handles the resulting data from the biometric capture
     */

    @TargetApi(Build.VERSION_CODES.M)
    public boolean hasPermissions(@NonNull String... permissions) {
        for (String permission : permissions)
            if (PackageManager.PERMISSION_GRANTED != checkSelfPermission(permission))
                return false;
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_APP_SETTINGS) {
            if (hasPermissions(requiredPermissions)) {
                ToastHelper.showMessage(LoginActivity.this, R.string.all_permissions_granted);
            } else {
                ToastHelper.showMessage(LoginActivity.this, R.string.permissions_not_granted);
            }
            recreate();
        } else {
            BiometricResultsParser.parse(resultCode, data, customCaptureHandler);
        }

        // complete
        super.onActivityResult(requestCode, resultCode, data);
    }


    /* Setup UI components and callbacks
     */
    public void onHomeFragmentReady(final HomeFragment homeFragment) {
        initSDK();

        // set version info
        if (mBiometricSDK != null) {
            homeFragment.tv_app_name.setText(String.format("%s %s", getString(R.string.app_name), BuildConfig.VERSION_NAME));
            homeFragment.tv_version_info.setText(String.format("%s (4F v%s)", mBiometricSDK.getVersionName(), FourFIntegrationWrapper.GetVersion()));
        }

        homeFragment.button_privacy_policy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                final Dialog dialog = new Dialog(LoginActivity.this);
//                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//                dialog.setContentView(R.layout.dialog_policy);
//
//                TextView info = (TextView) dialog.getWindow().getDecorView().findViewById(R.id.long_info);
//                info.setMovementMethod(new ScrollingMovementMethod());
//                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
//                    info.setTextAlignment(View.TEXT_ALIGNMENT_TEXT_START);
//                }
//
//                TextView heading = (TextView) dialog.getWindow().getDecorView().findViewById(R.id.heading);
//                heading.setText(R.string.privacy_policy);
//
//
//                Button continueInfo = (Button) dialog.getWindow().getDecorView().findViewById(R.id.button_continue_info);
//
//                continueInfo.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        dialog.dismiss();
//                    }
//                });
//
//
//                dialog.getWindow().getAttributes().windowAnimations = com.veridiumid.sdk.fourf.defaultui.R.style.DialogTheme;
//                dialog.setCancelable(true);
//                dialog.show();
            }
        });

        homeFragment.button_enroll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mBiometricSDK != null) {
                    Intent captureIntent = mBiometricSDK.enroll(FourFInterface.UID);
                    startActivityForResult(captureIntent, REQUEST_ENROLL);
                } else {
                    ToastHelper.showMessage(LoginActivity.this, R.string.engine_not_initialise);
                    Log.e(LOG_TAG, "IVeridiumSDK object not initialised");
                }
            }
        });

        homeFragment.button_enroll_export.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mBiometricSDK != null) {
                    Intent captureIntent = mBiometricSDK.enrollExport(FourFInterface.UID);
                    startActivityForResult(captureIntent, REQUEST_ENROLL_EXPORT);
                } else {
                    ToastHelper.showMessage(LoginActivity.this, R.string.engine_not_initialise);
                    Log.e(LOG_TAG, "IVeridiumSDK object not initialised");
                }
            }
        });

        homeFragment.button_authenticate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mBiometricSDK != null) {
                    Intent captureIntent = mBiometricSDK.authenticate(FourFInterface.UID);
                    startActivityForResult(captureIntent, REQUEST_AUTH);
                } else {
                    ToastHelper.showMessage(LoginActivity.this, R.string.engine_not_initialise);
                    Log.e(LOG_TAG, "IVeridiumSDK object not initialised");
                }
            }
        });

        // set up liveness checkbox, with event callback
        homeFragment.cb_liveness.setChecked(ExportConfig.getUseLiveness());
        homeFragment.cb_liveness.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                ExportConfig.setUseLiveness(isChecked);
            }
        });

        homeFragment.cb_debug.setChecked(ExportConfig.getPackDebugInfo());
        homeFragment.cb_debug.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                ExportConfig.setPackDebugInfo(isChecked);
            }
        });


        // Set current session id text
        tv_session_id = homeFragment.tv_session_id;
        updateSessionID();

        // Create a save directory
        File sdCard = Environment.getExternalStorageDirectory();
        saveDir = new File(sdCard.getAbsolutePath() + savePath);
        saveDir.mkdirs();
        homeFragment.tv_save_location.setText("Save directory: " + saveDir.getAbsolutePath());
    }

    private void initSDK() {
        try {
            mBiometricSDK = VeridiumSDK.getSingleton();
        } catch (LicenseException e) {
            ToastHelper.showMessage(LoginActivity.this, R.string.license_is_invalid);
            e.printStackTrace();
        }

        //Example UI Customization
//        UICustomization.setForegroundColor(Color.WHITE);
//        UICustomization.setBackgroundColor(0xff156441);
//        UICustomization.setDialogColor(0xffab2032);
//        UICustomization.setFingerColor(0xffBDBDBD);
        //   UICustomization.setLogo(getResources().getDrawable(R.drawable.veridium_logo_capture_screen));

    }

    /* Write a template to disk with session ID and timestamp
     *
     */
    private void writeTemplate(byte[] template) {
        String fileExtension = IBiometricFormats.TemplateFormat.getExtension(ExportConfig.getFormat());
        String fileName = String.format("TouchlessID_%03d_%d_%s%s", session_id, System.currentTimeMillis(), ExportConfig.getFormat(), fileExtension);
        Log.d(LOG_TAG, "Save template with " + fileName);
        Log.d(LOG_TAG, "Template byte size to disk: " + template.length);

        File file = new File(saveDir, fileName);
        try {
            FileOutputStream dos = new FileOutputStream(file);
            dos.write(template);
            dos.close();
            sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.fromFile(file))); // allow file to be seen via MTP
        } catch (Exception ex) {
            Log.d(LOG_TAG, "Failed to write template file");
            ex.printStackTrace();
        }

    }
}