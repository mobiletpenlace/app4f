package com.totalplay.fieldforcetp;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

import com.veridiumid.sdk.support.base.VeridiumBaseFragment;

public class HomeFragment extends VeridiumBaseFragment {

    private static final String LOG_TAG = HomeFragment.class.getSimpleName();

    public TextView tv_app_name;
    public TextView tv_version_info;
    public Button button_enroll;
    public Button button_enroll_export;
    public Button button_authenticate;
    public TextView tv_save_location;
    public TextView tv_session_id;
    public CheckBox cb_liveness;
    public CheckBox cb_debug;
    public Button button_privacy_policy;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_home, container, false);
    }

    @Override
    public void initView(View view) {
        tv_app_name = (TextView) view.findViewById(R.id.text_app_name);
        tv_version_info = (TextView) view.findViewById(R.id.text_app_version);
        button_enroll = (Button) view.findViewById(R.id.button_enroll);
        button_enroll_export = (Button) view.findViewById(R.id.button_enrol_export);
        button_authenticate = (Button) view.findViewById(R.id.button_authenticate);
        tv_save_location = (TextView) view.findViewById(R.id.tv_save_location);
        tv_session_id = (TextView) view.findViewById(R.id.tv_session_id);
        cb_liveness = (CheckBox) view.findViewById(R.id.cb_liveness);
        cb_debug = (CheckBox) view.findViewById(R.id.cb_debug);
        button_privacy_policy = (Button) view.findViewById(R.id.btn_privacy_policy);
        ((LoginActivity) getActivity()).onHomeFragmentReady(this);
    }
}
