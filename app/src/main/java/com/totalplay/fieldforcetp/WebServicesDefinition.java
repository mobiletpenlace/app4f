package com.totalplay.fieldforcetp;


import okhttp3.Response;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;


public interface WebServicesDefinition {

    @FormUrlEncoded
    @POST("/kivoxWeb/rest/finger/start-session")
    Call<ResponseBody> loginUserTotalPlay(@Field("enterpriseId") String enterpriseId, @Field("configuration") String configuration, @Field("device") String device);


}
