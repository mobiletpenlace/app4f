package com.totalplay.fieldforcetp;


import android.annotation.SuppressLint;
import android.util.Base64;

import com.ihsanbal.logging.Level;
import com.ihsanbal.logging.LoggingInterceptor;

import java.io.IOException;
import java.security.cert.CertificateException;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import okhttp3.Headers;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.internal.platform.Platform;
import retrofit2.Retrofit;


/***
 */

public class WebServices {
    private static WebServicesDefinition servicesDefinition;


    public static WebServicesDefinition services() {
        if (servicesDefinition == null) {
            setupServicesDefinition(false);
        }
        return servicesDefinition;
    }


    public static void switchEndpoint() {
        setupServicesDefinition(false);
    }

    public static WebServicesDefinition services(boolean isECommerce) {
        if (servicesDefinition == null) {
            setupServicesDefinition(isECommerce);
        }
        return servicesDefinition;
    }

    private static void setupServicesDefinition(final boolean isECommerce) {
        OkHttpClient.Builder builder = new OkHttpClient().newBuilder();
        builder.readTimeout(120, TimeUnit.SECONDS);
        builder.connectTimeout(120, TimeUnit.SECONDS);

        builder.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request.Builder builder = chain.request().newBuilder();
                builder.headers(getJsonHeader(isECommerce));
                return chain.proceed(builder.build());
            }
        });

        String ws = "https://13.58.109.129";


        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ws)
                .client(getUnsafeOkHttpClient(builder))
                // .addConverterFactory(GsonConverterFactory.create(GsonUtils.gsonForDeserialization()))
                .build();

        servicesDefinition = retrofit.create(WebServicesDefinition.class);
    }


    private static Headers getJsonHeader(boolean isECommerce) {
        String username, pwd;
        if (isECommerce) {
            username = "ffmapp";
            pwd = "4gend4mi3nto";
        } else {
            username = "oaguser";
            pwd = "o4gus3r";
        }

        String credentials = username + ":" + pwd;
        final String basic = "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
        Headers.Builder builder = new Headers.Builder();
        builder.add("Content-Type", "application/json");
        builder.add("Accept", "application/json");
        builder.add("Authorization", basic);
        return builder.build();
    }

    @SuppressLint("TrustAllX509TrustManager")
    private static OkHttpClient getUnsafeOkHttpClient(OkHttpClient.Builder builder) {

        try {
            // Create a trust manager that does not validate certificate chains
            final TrustManager[] trustAllCerts = new TrustManager[]{
                    new X509TrustManager() {

                        @Override
                        public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                        }

                        @Override
                        public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                        }

                        @Override
                        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                            return new java.security.cert.X509Certificate[]{};
                        }
                    }
            };

            // Install the all-trusting trust manager
            final SSLContext sslContext = SSLContext.getInstance("SSL");
            sslContext.init(null, trustAllCerts, new java.security.SecureRandom());
            // Create an ssl socket factory with our all-trusting manager
            final SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();


            builder.sslSocketFactory(sslSocketFactory);
            builder.hostnameVerifier(new HostnameVerifier() {
                @SuppressLint("BadHostnameVerifier")
                @Override
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            });
            if (BuildConfig.DEBUG) {
                builder.addInterceptor(new LoggingInterceptor.Builder()
                        .loggable(BuildConfig.DEBUG)
                        .setLevel(Level.BASIC)
                        .log(Platform.INFO)
                        .request("Request")
                        .response("Response")
                        .addHeader("version", BuildConfig.VERSION_NAME)
                        .build());
            }

            return builder.build();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }


    }
}