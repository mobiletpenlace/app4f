package com.totalplay.fieldforcetp;


import android.os.Bundle;

import com.veridiumid.sdk.core.data.persistence.IKVStore;
import com.veridiumid.sdk.defaultdata.DataStorage;
import com.veridiumid.sdk.fourf.defaultui.activity.DefaultFourFBiometricsActivity;

public class CustomFourFBiometricsActivity extends DefaultFourFBiometricsActivity {

    private static final String LOG_TAG = CustomFourFBiometricsActivity.class.getName();

    private final int FOURF_TIMEOUT = 120000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTimeout(FOURF_TIMEOUT);
    }

    //@Override
    //public void onProcessingStart() {
        // Vibrate when moving to the processing screen to indicate capture completion.
        // showFragment(new MyFourFWaitForProcessingFragment());
    //}


    // override the openStorage() to change where and how data is saved.
    @Override
    protected IKVStore openStorage() {
        return DataStorage.getDefaultStorage();
    }


    // To access and restore enrollment data externally, create an
    // InMemoryKVStore, which will hold all data in memory, and override the
    // openStorage() call. Data can then be read and updated
/*
    private static InMemoryKVStore myMemoryKVStore = new InMemoryKVStore();
    private static String ENROL_KEY_LEFT =  "enrollment_" + FourFInterface.SUFFIX_KEY_ENROL_LEFT;
    private static String ENROL_KEY_RIGHT =  "enrollment_" + FourFInterface.SUFFIX_KEY_ENROL_LEFT;

    @Override
    protected IKVStore openStorage() {
        return myMemoryKVStore;
    }

    // Extract 4F enrollment data as a byte[]
    public byte[] getEnrolData(String Key){
        if(myMemoryKVStore!=null){
            if(myMemoryKVStore.contains(Key)){
                byte[] data = myMemoryKVStore.read(Key);
                if (data != null && data.length > 0) {
                    return data;
                }
            }
        }
        return null;
    }

    // Insert 4F enrolment data for authentication
    public void setEnrolData(byte[] data, String key){
        if(myMemoryKVStore!=null){
            myMemoryKVStore.purge();
            myMemoryKVStore.update(key, data);
        }
    }
*/
}