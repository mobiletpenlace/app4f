package com.totalplay.fieldforcetp;


import android.app.Application;
import android.content.Context;

import com.veridiumid.sdk.SDKInitializationException;
import com.veridiumid.sdk.VeridiumSDK;
import com.veridiumid.sdk.defaultdata.VeridiumDataInitializer;
import com.veridiumid.sdk.fourf.VeridiumFourFInitializer;

public class App extends Application {


    @Override
    public void onCreate() {
        super.onCreate();
        Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
            @Override
            public void uncaughtException(Thread thread, Throwable ex) {
                ex.printStackTrace();
                System.exit(0);
            }
        });

        Context appContext = getApplicationContext();


        try {
            VeridiumSDK.init(appContext,
                    new VeridiumFourFInitializer(),
                    new VeridiumDataInitializer()
            );
        } catch (SDKInitializationException e) {
            e.printStackTrace();
        }


    }


}

